﻿#pragma warning disable 0649
using UnityEngine;
using System.Collections;

public class RTSBase : MonoBehaviour {

    private bool _isInitialised = false;
    protected bool isInitialised { get { return _isInitialised; } }

    private void Awake()
    {
        _isInitialised = Initialise();
    }

    private void OnDestroy()
    {
        _isInitialised = false;
    }
    
	protected virtual bool Initialise()
    {
        return true;
    }
}
