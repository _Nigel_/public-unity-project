﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using RTSGame.Units;

namespace RTSGame.Players
{
    public enum Behaviour { Aggressive, Defencive, Passive }

    public class AIController : PlayerController
    {
        private bool isPlaying = true;
        
        protected override bool Initialise()
        {
            StartCoroutine(AIBehaviour());
            return base.Initialise();
        }

        private IEnumerator AIBehaviour()
        {
            while(isPlaying)
            {
                //if (player != null && player.targets.Count > 0)
                //{
                //   for(int i=0; i < player.units.Count; i++)
                //    {
                //        player.units[i].NewCommand(player.targets[0]);
                //    } 
                //}

                yield return new WaitForSeconds(RTSUniversalSettings.timeStep);
            }
        }
    }
}