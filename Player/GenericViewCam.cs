﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Players
{
    public class GenericViewCam : RTSBase, IPossessable
    {
        [SerializeField] float height;
        [SerializeField] Vector2 heightBounds;
        [SerializeField] float zoomSpeed;
        [SerializeField] LayerMask layerMask;
        private Vector3 velocity = Vector3.zero;
        private float smoothDamp = 0.35f;
        private bool isPossessed = false;
        private Vector3 startDragPosition = Vector3.zero;

        public float possessionCost { get { return 0; } }

        public bool possess
        {
            get
            {
                return isPossessed;
            }
            set
            {
                isPossessed = value;

                if (isPossessed)
                {
                    transform.parent = PlayerControls.player.transform;
                    transform.localPosition = Vector3.zero;
                }
            }
        }

        public void UpdateControls()
        {
            float forward = Input.GetAxis("Vertical");
            float horizontal = Input.GetAxis("Horizontal");
            float vertical = Input.GetAxis("Mouse ScrollWheel");

            float dot = Vector3.Dot(transform.forward, Vector3.up);

            Vector3 flatForward;
            if (Mathf.Abs(dot) > 0.75f)
            {
                flatForward = transform.up.normalized;
            }
            else
            {
                flatForward = transform.forward.normalized;
            }

            flatForward.y = 0;

            transform.root.position = Vector3.SmoothDamp(transform.position, transform.position + (flatForward * forward * InputSettings.moveSensitivity), ref velocity, smoothDamp * Time.deltaTime);
            transform.root.position = Vector3.SmoothDamp(transform.position, transform.position + (transform.right * horizontal * InputSettings.moveSensitivity), ref velocity, smoothDamp * Time.deltaTime);
            
            height = Mathf.Clamp(height + (vertical * zoomSpeed), heightBounds.x, heightBounds.y);
            
            Ray ray = new Ray(transform.position, Vector3.down);
            RaycastHit hit;

            if(Physics.Raycast(ray, out hit, 500, layerMask))
            {
                Vector3 heightAdj = transform.root.position;
                heightAdj.y = hit.point.y + height;

                transform.root.position = Vector3.SmoothDamp(transform.root.position, heightAdj, ref velocity, smoothDamp * Time.deltaTime);
            }

            if(Input.GetMouseButtonDown(1))
            {
                startDragPosition = Input.mousePosition;
            }
            if(Input.GetMouseButton(1))
            {
                float differenceY = Input.mousePosition.y - startDragPosition.y;
                float differenceX = Input.mousePosition.x - startDragPosition.x;
                startDragPosition = Input.mousePosition;

                transform.root.Rotate(Vector3.up, differenceX, Space.World);
                transform.root.Rotate(transform.right, -differenceY, Space.World);
            }
        }

        public void UpdateCamera()
        {
            // nothing needed in this one
        }
    }
}