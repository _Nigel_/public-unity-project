﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RTSGame.Units;
using RTSGame.UI;

namespace RTSGame.Players
{
    public static class InputSettings
    {
        public static float mouseSpeed = 2.5f;
        public static float moveSensitivity = 2.5f;
        public static float zoomSensitivity = 1.0f;
    }

    public class PlayerControls : PlayerController
    {
        public delegate void SelectHandler(ISelectable iSelectable);
        public static event SelectHandler Select;
        public static event SelectHandler Deselect;
        
        private static Rect _selectionRect;
        private static List<ISelectable> _selectedUnits = new List<ISelectable>();
        private static PlayerControls playerControls;

        [SerializeField] private static Player _localPlayer;
        [SerializeField] private LayerMask layerMask;
        [SerializeField] private RectTransform selectionBoxUI;
        [SerializeField] private float dragThreshold = 0.5f;
        [SerializeField] private static bool _isDraggingLeft = false, _isDraggingRight = false;
        [SerializeField] private float doubleClickThreshold, clickTime = 10, lastClickTime = 10;
        private Vector3 startLeftDragPosition = Vector3.zero, startRightDragPosition = Vector3.zero;

        public static new Player player { get { return _localPlayer; } }
        public static Rect selectionRect { get { return _selectionRect; } }
        public static ReadOnlyCollection<ISelectable> selectedUnits { get { return _selectedUnits.AsReadOnly(); } }
        public static bool isDraggingRight { get { return _isDraggingRight; } }

        protected override bool Initialise()
        {
            if(playerControls == null)
            {
                playerControls = this;
            }
            else
            {
                Destroy(this);
            }

            _localPlayer = transform.root.GetComponentInChildren<Player>();

            return base.Initialise();
        }

        void OnDestroy()
        {
            if(Select != null)
            {
                Select = null;
            }
            if(Deselect != null)
            {
                Deselect = null;
            }
        }

        public static void RemoveSelected(ISelectable selectable)
        {
            _selectedUnits.Remove(selectable);

            if(Deselect != null)
            {
                Deselect(selectable);
            }
        }

        public static void RemoveAllSelected()
        {
            for(int i=0; i < _selectedUnits.Count; i++)
            {
                Deselect(_selectedUnits[i]);
            }
            _selectedUnits.Clear();
        }

        public static void AddSelected(ISelectable selectable)
        {
            if(!_selectedUnits.Contains(selectable))
            {
                _selectedUnits.Add(selectable);

                if(Select != null)
                {
                    Select(selectable);
                }
            }
        }

        public static float InvertMouseY(float y)
        {
            return Screen.height - y;
        }
        
        private void Update()
        {
            if(clickTime < doubleClickThreshold)
            {
                clickTime += Time.deltaTime;
            }

            LeftClick();
            RightClick();
            CheckKeys();

            if (player.possessedUnit != null)
            {
                player.possessedUnit.UpdateControls();
                player.possessedUnit.UpdateCamera();
            }
        }

        private void CheckKeys()
        {
            if(Input.GetButtonDown("Cancel"))
            {
                if(player.possessedUnit != player.defaultPossessedUnit)
                {
                    player.UnpossessUnit();
                }
                else
                {
                    if(Time.timeScale == 1)
                    {
                        Time.timeScale = 0;
                    }
                    else
                    {
                        Time.timeScale = 1;
                    }
                }
            }
        }

        #region LeftClick
        private void LeftClick()
        {
            if (_isDraggingRight)
            {
                return;
            }
            
            if (Input.GetMouseButtonDown(0))
            {
                LeftMouseDown();
            }
            
            if (Input.GetMouseButton(0))
            {
                LeftMouseHeld();
            }
            
            if (Input.GetMouseButtonUp(0))
            {
                LeftMouseUp();
            }
        }

        private void LeftMouseDown()
        {
            lastClickTime = clickTime;
            clickTime = 0;

            PlayerControls.RemoveAllSelected();

            _isDraggingLeft = false;
            _selectionRect = default(Rect);
            startLeftDragPosition = Input.mousePosition;
        }

        private void LeftMouseHeld()
        {
            float differenceY = Input.mousePosition.y - startLeftDragPosition.y;
            float differenceX = Input.mousePosition.x - startLeftDragPosition.x;

            if ((Mathf.Abs(differenceX) + Mathf.Abs(differenceY)) > dragThreshold)
            {
                _isDraggingLeft = true;

                _selectionRect = new Rect(startLeftDragPosition.x, InvertMouseY(startLeftDragPosition.y), Input.mousePosition.x - startLeftDragPosition.x, InvertMouseY(Input.mousePosition.y) - InvertMouseY(startLeftDragPosition.y));

                if (_selectionRect.width < 0)
                {
                    _selectionRect.x += _selectionRect.width;
                    _selectionRect.width = -_selectionRect.width;
                }
                if (_selectionRect.height < 0)
                {
                    _selectionRect.y += _selectionRect.height;
                    _selectionRect.height = -_selectionRect.height;
                }

                PlayerUI.playerUI.selectionBox.position = new Vector2(_selectionRect.x, InvertMouseY(_selectionRect.y + _selectionRect.height));
                PlayerUI.playerUI.selectionBox.sizeDelta = new Vector2(_selectionRect.width, _selectionRect.height);
            }
        }

        private void LeftMouseUp()
        {
            PlayerUI.playerUI.selectionBox.sizeDelta = new Vector2(0, 0);
            PlayerUI.playerUI.selectionBox.position = new Vector2(0, 0);

            if (_isDraggingLeft)
            {
                _isDraggingLeft = false;
            }
            else
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                if ((clickTime + lastClickTime) < doubleClickThreshold)
                {
                    IPossessable possessable;

                    if (Physics.Raycast(ray, out hit, 500, layerMask))
                    {
                        if (player.units.Contains(hit.collider.transform.root.GetComponentInChildren<IUnitController>()))
                        {
                            possessable = hit.collider.transform.root.GetComponentInChildren<IPossessable>();

                            if (possessable != null)
                            {
                                if (player.capacitor.AvailableEnergy(possessable.possessionCost))
                                {
                                    if (player.capacitor.UseEnergy(possessable.possessionCost))
                                    {
                                        player.PossessUnit(possessable);
                                    }
                                }
                            }
                        }
                    }
                }
                else
                {
                    ISelectable selectable;

                    if (Physics.Raycast(ray, out hit, 500, layerMask))
                    {
                        selectable = hit.collider.transform.root.GetComponentInChildren<ISelectable>();

                        if (selectable != null && !_selectedUnits.Contains(selectable) && player.units.Contains(selectable.unit.controller))
                        {
                            PlayerControls.AddSelected(selectable);
                        }
                    }
                }
            }
        }
        #endregion

        #region RightClick
        private void RightClick()
        {
            if (_isDraggingLeft)
            {
                return;
            }

            if (Input.GetMouseButtonDown(1))
            {
                RightMouseDown();
            }
            if (Input.GetMouseButton(1))
            {
                RightMouseHeld();
            }
            if (Input.GetMouseButtonUp(1))
            {
                RightMouseUp();
            }
        }

        private void RightMouseDown()
        {
            _isDraggingRight = false;
            startRightDragPosition = Input.mousePosition;
        }

        private void RightMouseHeld()
        {
            float differenceY = Input.mousePosition.y - startRightDragPosition.y;
            float differenceX = Input.mousePosition.x - startRightDragPosition.x;

            if ((Mathf.Abs(differenceX) + Mathf.Abs(differenceY)) > dragThreshold)
            {
                _isDraggingRight = true;
                Cursor.lockState = CursorLockMode.Confined;
                Cursor.visible = false;
            }

            startRightDragPosition = Input.mousePosition;
        }

        private void RightMouseUp()
        {
            if (!_isDraggingRight)
            {
                HandleCommands();
            }
            else
            {
                _isDraggingRight = false;
                Cursor.lockState = CursorLockMode.None;
                Cursor.visible = true;
            }
        }

        private void HandleCommands()
        {
            if (_selectedUnits.Count > 0)
            {
                RaycastHit hit;
                Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);

                IDamageable damageable;
                IUnit unit;

                if (Physics.Raycast(ray, out hit, 1500, layerMask))
                {
                    damageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();
                    unit = hit.collider.transform.root.GetComponentInChildren<IUnit>();

                    Vector3 modifiedPoint = hit.point;
                    for (int i = 0; i < _selectedUnits.Count; i++)
                    {
                        if (unit != null)
                        {
                            _selectedUnits[i].GiveCommand(unit);
                        }
                        else if (damageable != null)
                        {
                            _selectedUnits[i].GiveCommand(damageable);
                        }
                        else
                        {
                            _selectedUnits[i].GiveCommand(modifiedPoint);

                            if (i % 5 == 0)
                            {
                                modifiedPoint.z += 10;
                                modifiedPoint.x = hit.point.x;
                            }
                            else
                            {
                                modifiedPoint.x += 5;
                            }
                        }
                    }
                }
            }
        }
        #endregion
    }
}