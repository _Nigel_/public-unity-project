﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RTSGame.Units;

namespace RTSGame.Players
{
    public enum PlayerType { Local_Human, External_Human, Virtual }

    public class Player : RTSBase, IPlayer
    {
        [SerializeField] private string[] startingUnits;
        [SerializeField] private int _playerID;
        [SerializeField] private string _teamName;
        [SerializeField] private int _teamNumber;
        [SerializeField] private Color _playerColor;
        [SerializeField] private LayerMask spawnLayerMask;
        
        private IPlayerController playerController;
        private ICapacitor _capacitor;
        private List<IUnitController[]> _squads = new List<IUnitController[]>();
        private List<IUnit> _targets = new List<IUnit>();
        private List<IUnitController> _units = new List<IUnitController>();
        private IPossessable _possessedUnit, _defaultPossessed;

        public ReadOnlyCollection<IUnit> targets { get { return _targets.AsReadOnly(); } }
        public ReadOnlyCollection<IUnitController> units { get { return _units.AsReadOnly(); } }
        public ReadOnlyCollection<IUnitController[]> squads { get { return _squads.AsReadOnly(); } }
        public IPlayerController controller { get { return playerController; } }
        public int playerID { get { return _playerID; } }
        public int team { get { return _teamNumber; } }
        public Color playerColor { get { return _playerColor; } }
        public IPossessable possessedUnit { get { return _possessedUnit; } }
        public IPossessable defaultPossessedUnit { get { return _defaultPossessed; } }
        public GameObject obj { get { if (this) { return gameObject; } else { return null; } } }

        public ICapacitor capacitor{ get { return _capacitor; } }
        
        protected override bool Initialise()
        {
            playerController = transform.root.GetComponentInChildren<IPlayerController>();
            _capacitor = transform.root.GetComponentInChildren<ICapacitor>();

            _possessedUnit = transform.root.GetComponentInChildren<IPossessable>();
            _defaultPossessed = _possessedUnit;

            return base.Initialise();
        }

        public void PossessUnit(IPossessable newUnit)
        {
            possessedUnit.possess = false;
            _possessedUnit = newUnit;
            possessedUnit.possess = true;
        }

        public void UnpossessUnit()
        {
            possessedUnit.possess = false;
            _possessedUnit = _defaultPossessed;
            possessedUnit.possess = true;
        }

        public void PlayerSetup(int newPlayerID)
        {
            _playerID = newPlayerID;

            LoadUnits(startingUnits, transform.position);
        }

        public void LoadUnit(string unitName)
        {
            Vector3 spawnPosition = transform.position;

            GameObject unit = UnitFactory.CreateUnit(unitName, team);

            Ray ray = new Ray(spawnPosition, Vector3.down * 2000);
            RaycastHit hit;

            if (Physics.Raycast(ray, out hit, 2000, spawnLayerMask))
            {
                if (unit != null)
                {
                    unit.transform.position = hit.point;
                }
            }
            else
            {
                if (unit != null)
                {
                    unit.transform.position = spawnPosition;
                }
            }

            ConsumeSpawnEnergy(unit);

            IUnitController unitController = unit.GetComponentInChildren<IUnitController>();
            if (unitController != null)
            {
                _units.Add(unitController);
                unitController.SetOwner(this as IPlayer);
                unitController.activeController = true;
                unitController.unit.Destroyed += LostUnit;
            }
        }
        
        public void LoadUnits(string[] unitList, Vector3 startingPoint)
        {
            Vector3 spawnPosition = startingPoint;
            float yOffset = 0;

            for (int i = 0; i < unitList.Length; i++)
            {
                GameObject unit = UnitFactory.CreateUnit(unitList[i], team);
                Ray ray = new Ray(spawnPosition, Vector3.down * 2000);
                RaycastHit hit;
                
                if (Physics.Raycast(ray, out hit, 2000, spawnLayerMask))
                {
                    if (unit != null)
                    {
                        unit.transform.position = hit.point;
                    }
                }
                else
                {
                    if (unit != null)
                    {
                        unit.transform.position = spawnPosition;
                    }
                }
                
                IUnitController unitController = unit.transform.root.GetComponentInChildren<IUnitController>();
                if(unitController != null)
                {
                    _units.Add(unitController);
                    unitController.SetOwner(this as IPlayer);
                    unitController.activeController = true;
                    unitController.unit.Destroyed += LostUnit;
                }

                if (i % 5 == 0)
                {
                    spawnPosition = startingPoint;
                    yOffset += 10;
                    spawnPosition.z += yOffset;
                }
                else
                {
                    spawnPosition.x += 10;
                }
            }
        }

        public void ConsumeSpawnEnergy(GameObject unit)
        {
            IEnergyCost[] expenditures = unit.transform.root.GetComponentsInChildren<IEnergyCost>();
            float creationCost = 0;
            for (int expLength = 0; expLength < expenditures.Length; expLength++)
            {
                creationCost += expenditures[expLength].creationCost;
            }
            capacitor.UseEnergy(creationCost);
        }

        public void AddTarget(IUnit newTarget)
        {
            if(!_targets.Contains(newTarget))
            {
                _targets.Add(newTarget);
            }
        }

        public void LostUnit(IUnit unitLost)
        {
            unitLost.Destroyed -= LostUnit;

            if(units.Contains(unitLost.controller))
            {
                _units.Remove(unitLost.controller);
            }

            //#if UNITY_EDITOR
            //Debug.Log(string.Format("Player {0} lost {1}", playerID, unitLost.controller));

            //if(units.Count <= 0)
            //{
            //    Debug.Log(string.Format("Player {0} Destroyed", playerID));
            //}
            //#endif
        }
    }
}