﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RTSGame.Units;

namespace RTSGame.Players
{
    public class PlayerController : RTSBase, IPlayerController
    {
        [SerializeField] private IPlayer _player;
        public IPlayer player { get { return _player; } }

        protected override bool Initialise()
        {
            _player = transform.root.GetComponentInChildren<IPlayer>();

            if (_player == null)
            {
                Debug.Log("AIController::Initialise - Player was not initialised with Controller!");
            }

            return base.Initialise();
        }
    }
}