﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RTSGame.Units;

namespace RTSGame.UI
{
    public class UnitUI : RTSBase, IPoolable
    {
        [SerializeField] public Gradient healthBarColor;
        [SerializeField] private RectTransform rectTransform;
        [SerializeField] private Image selectionBox;
        [SerializeField] private Image healthBar;
        [SerializeField] private Image levelIcon;
        [SerializeField] private Sprite[] levelSprites;
        
        protected override bool Initialise()
        {
            return base.Initialise();
        }

        public void Activate()
        {
            if (rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }

            gameObject.SetActive(true);
        }

        public void Activate(Vector3 position, Vector3 rotation)
        {
            if(rectTransform == null)
            {
                rectTransform = GetComponent<RectTransform>();
            }
            
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            DeselectUnit();
            gameObject.SetActive(false);
        }

        public void SetTransform(IUnit iUnit)
        {
            float evenSize = Mathf.Max(25,25);
            
            if(iUnit.unitObject == null)
            {
                Deactivate();
            }
            else
            {
                rectTransform.position = Camera.main.WorldToScreenPoint(iUnit.unitObject.transform.position);
                rectTransform.sizeDelta = new Vector2(evenSize, evenSize);
            }
        }

        public void SetColor(Color newColor)
        {
            GradientColorKey[] gck;
            GradientAlphaKey[] gak;
            gck = new GradientColorKey[3];
            gck[0].color = Color.red;
            gck[0].time = 0.0F;
            gck[1].color = newColor;
            gck[1].time = 1.0F;
            gak = new GradientAlphaKey[2];
            gak[0].alpha = 1.0F;
            gak[0].time = 0.0F;
            gak[1].alpha = 1.0F;
            gak[1].time = 1.0F;
            healthBarColor.SetKeys(gck, gak);
        }

        public void SetLevel(int level)
        {
            levelIcon.sprite = levelSprites[level];
        }

        public void SelectUnit()
        {
            selectionBox.enabled = true;
        }

        public void DeselectUnit()
        {
            selectionBox.enabled = false;
        }

        public Rect BoundsToScreenRect(Bounds bounds)
        {
            Vector3 origin = Camera.main.WorldToScreenPoint(new Vector3(bounds.min.x, bounds.max.y, 0f));
            Vector3 extent = Camera.main.WorldToScreenPoint(new Vector3(bounds.max.x, bounds.min.y, 0f));
            return new Rect(origin.x, Screen.height - origin.y, extent.x - origin.x, origin.y - extent.y);
        }

        public void SetHealth(float percentageHealth)
        {
            Mathf.Clamp(percentageHealth, 0, 100);
            percentageHealth /= 100;
            healthBar.color = healthBarColor.Evaluate(percentageHealth);
            healthBar.fillAmount = percentageHealth;
        }
    }
}