﻿using UnityEngine;
using System.Collections;

namespace RTSGame.UI
{
    [RequireComponent(typeof(Camera))]
    public class MiniMapUI : RTSBase
    {
        [SerializeField] private float heightOffset;

        protected override bool Initialise()
        {
            return base.Initialise();
        }
        
        void LateUpdate()
        {
            Vector3 updatedPosition = transform.parent.position;
            updatedPosition.y += heightOffset;
            transform.position = updatedPosition;
            transform.LookAt(transform.parent);
        }
    }
}
