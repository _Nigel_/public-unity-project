﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using RTSGame.Units;
using RTSGame.Players;
using Vectrosity;

namespace RTSGame.UI
{
    public class PlayerUI : RTSBase
    {
        private static PlayerUI _playerUI;
        private static Canvas _mainCanvas;
        [SerializeField] private Material navLineMaterial;
        [SerializeField] private Image energyBar;
        [SerializeField] private Gradient energyColor;
        [SerializeField] private Transform unitUIContainer;
        [SerializeField] private GameObject unitUIOverlay;
        [SerializeField] private RectTransform _selectionBox;
        private Dictionary<IUnit, UnitUI> onScreenUnits = new Dictionary<IUnit, UnitUI>();
        
        public static PlayerUI playerUI
        {
            get
            {
                if (_playerUI == null)
                {
                    _mainCanvas = Resources.Load("MainCanvas") as Canvas;
                    if(_mainCanvas != null)
                    {
                        _playerUI = _mainCanvas.GetComponent<PlayerUI>();
                        _playerUI.Initialise();
                    }
                }
                else if ((System.Object)_playerUI == null)
                {
                    return null;
                }
                else { } // do nothing

                return _playerUI;
            }
        }

        public RectTransform selectionBox { get { return _selectionBox; } }
        public Canvas mainCanvas { get { if (!isInitialised) Initialise(); return _mainCanvas; } }
        public GameObject unitUI { get { return unitUIOverlay; } }

        protected override bool Initialise()
        {
            if (_playerUI == null)
            {
                _playerUI = this;
                _mainCanvas = GetComponent<Canvas>();
            }

            PlayerControls.Deselect += DeselectUnit;
            PlayerControls.Select += SelectUnit;

            return base.Initialise();
        }

        public void SelectUnit(ISelectable selected)
        {
            if(onScreenUnits.ContainsKey(selected.unit))
            {
                onScreenUnits[selected.unit].SelectUnit();
            }
        }
        
        public void DeselectUnit(ISelectable deselected)
        {
            if (onScreenUnits.ContainsKey(deselected.unit))
            {
                onScreenUnits[deselected.unit].DeselectUnit();
            }
        }

        public void AddUnit(IUnit iUnit)
        {
            if(!onScreenUnits.ContainsKey(iUnit))
            {
                onScreenUnits.Add(iUnit, ObjectPool.GetObject(unitUIOverlay, unitUIContainer).GetComponent<UnitUI>());
                onScreenUnits[iUnit].Activate();
                onScreenUnits[iUnit].SetColor(iUnit.team == PlayerControls.player.team ? Color.green : Color.red);
                onScreenUnits[iUnit].SetTransform(iUnit);
                onScreenUnits[iUnit].SetLevel(iUnit.experiencer.currentLevel);
                
                ISelectable selectable = iUnit.unitObject.transform.root.GetComponentInChildren<ISelectable>();
                if(selectable != null && PlayerControls.selectedUnits.Contains(selectable))
                {
                    onScreenUnits[iUnit].SelectUnit();
                }

                iUnit.experiencer.LevelUp += DisplayLevelUp;
                iUnit.Damaged += UpdateHealthBar;
                iUnit.Destroyed += RemoveUnit;
                UpdateHealthBar(iUnit);
            }
        }
        
        public void RemoveUnit(IUnit iUnit)
        {
            if(onScreenUnits.ContainsKey(iUnit))
            {
                iUnit.Damaged -= UpdateHealthBar;
                iUnit.Destroyed -= RemoveUnit;
                iUnit.experiencer.LevelUp -= DisplayLevelUp;
                onScreenUnits[iUnit].Deactivate();
                onScreenUnits.Remove(iUnit);
            }
        }

        public void DisplayLevelUp(IUnit unit, int newLevel)
        {
            if (onScreenUnits.ContainsKey(unit))
            {
                onScreenUnits[unit].SetLevel(newLevel);
            }
        }

        public void UpdateHealthBar(IUnit iUnit)
        {
            float percentage = iUnit.health / iUnit.maxHealth;
            percentage *= 100;
            Mathf.Clamp(percentage, 0, 100);
            onScreenUnits[iUnit].SetHealth(percentage);
        }

        void LateUpdate()
        {
            if(energyBar != null && PlayerControls.player && PlayerControls.player.capacitor != null)
            {
                float capacityFilled = PlayerControls.player.capacitor.energy / PlayerControls.player.capacitor.capacity;
                energyBar.color = energyColor.Evaluate(capacityFilled);
                energyBar.fillAmount = (capacityFilled);
            }

            foreach (KeyValuePair<IUnit, UnitUI> unit in onScreenUnits)
            {
                if (unit.Key != null && unit.Key.unitObject != null && unit.Value != null)
                {
                    UpdateUnitUI(unit);
                }
            }
        }

        public void UpdateUnitUI(KeyValuePair<IUnit, UnitUI> unit)
        {
            if(unit.Key != null && unit.Key.unitObject != null)
            {
                unit.Value.transform.position = Camera.main.WorldToScreenPoint(unit.Key.unitObject.transform.position);
            }
            else
            {
                RemoveUnit(unit.Key);
            }
        }

        public void DrawNavigationLine(ICommand command, IUnit unit, Vector3[] path)
        {
            if(command.commandType == CommandType.Attack)
            {
                StartCoroutine(DrawLines(path, Color.red));
            }
            else if(command.commandType == CommandType.Move)
            {
                StartCoroutine(DrawLines(path, Color.green));
            }
            else if(command.commandType == CommandType.Follow || command.commandType == CommandType.Guard)
            {
                StartCoroutine(DrawLines(path, Color.cyan));
            }
            else
            {
                StartCoroutine(DrawLines(path, Color.green));
            }
        }

        private IEnumerator DrawLines(Vector3[] path, Color color)
        {
            List<Vector3> someList = new List<Vector3>();
            someList.AddRange(path);
            VectorLine line = new VectorLine("NavigationLine", someList, navLineMaterial.mainTexture, 5, LineType.Discrete);
            line.color = color;
            line.Draw3DAuto();

            yield return new WaitForSeconds(0.75f);
            VectorLine.Destroy(ref line);
        }
        
        private void OnDestroy()
        {
            PlayerControls.Deselect -= DeselectUnit;
            PlayerControls.Select -= SelectUnit;

            if (_playerUI != null)
            {
                _playerUI = null;
            }
        }
    }
}