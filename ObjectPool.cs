﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame
{
    public class ObjectPool : RTSBase
    {
        private static ObjectPool _instance;
        private int poolCacheQuantity = 20;
        private List<GameObject> poolContainers = new List<GameObject>();
        private Dictionary<string, List<GameObject>> pools = new Dictionary<string, List<GameObject>>();
        private bool expandingPools = true;

        public static ObjectPool instance
        {
            get
            {
                if (_instance == null)
                {
                    GameObject newInstance = new GameObject("ObjectPool");
                    newInstance.AddComponent<ObjectPool>();
                    _instance = newInstance.GetComponent<ObjectPool>();
                    DontDestroyOnLoad(newInstance);
                }

                return _instance;
            }
        }

        private void OnDestroy()
        {
            _instance = null;
        }

        public static GameObject GetObject(GameObject poolType)
        {
            string name = poolType.name;

            if(!instance.pools.ContainsKey(name))
            {
                instance.pools.Add(name, instance.NewPool(poolType));
            }

            foreach(KeyValuePair<string, List<GameObject>> pool in instance.pools)
            {
                if(pool.Key == name)
                {
                    int i;
                    for(i=0; i < pool.Value.Count; i++)
                    {
                        if(!pool.Value[i].activeSelf)
                        {
                            return pool.Value[i];
                        }
                    }
                    
                    if(instance.expandingPools)
                    {
                        GameObject poolObj = Instantiate(poolType, Vector3.zero, Quaternion.identity) as GameObject;
                        poolObj.name = poolType.name + i.ToString();

                        foreach(GameObject poolContainer in instance.poolContainers)
                        {
                            if(poolContainer.name == poolType.name + "_Container")
                            {
                                poolObj.transform.SetParent(poolContainer.transform);
                            }
                        }

                        poolObj.SetActive(false);
                        pool.Value.Add(poolObj);
                        return poolObj;
                    }
                }
            }
            
            return null;
        }

        public static GameObject GetObject(GameObject poolType, Transform objParent)
        {
            string name = poolType.name;

            if (!instance.pools.ContainsKey(name))
            {
                instance.pools.Add(name, instance.NewPool(poolType, objParent));
            }

            foreach (KeyValuePair<string, List<GameObject>> pool in instance.pools)
            {
                if (pool.Key == name)
                {
                    int i;
                    for (i = 0; i < pool.Value.Count; i++)
                    {
                        if (!pool.Value[i].activeSelf)
                        {
                            return pool.Value[i];
                        }
                    }

                    if (instance.expandingPools)
                    {
                        GameObject poolObj = Instantiate(poolType, Vector3.zero, Quaternion.identity) as GameObject;
                        poolObj.name = poolType.name + i.ToString();
                        
                        foreach (GameObject poolContainer in instance.poolContainers)
                        {
                            if (poolContainer.name.Contains(objParent.gameObject.name))
                            {
                                poolObj.transform.SetParent(poolContainer.transform);
                            }
                        }

                        poolObj.SetActive(false);
                        pool.Value.Add(poolObj);
                        return poolObj;
                    }
                }
            }

            return null;
        }

        private List<GameObject> NewPool(GameObject _poolType)
        {
            if (!poolContainers.Contains(_poolType))
            {
                List<GameObject> objectPool = new List<GameObject>();
                GameObject newPoolContainer = new GameObject(_poolType.name + "_Container");
                newPoolContainer.transform.SetParent(instance.gameObject.transform);
                poolContainers.Add(newPoolContainer);

                for (int i = 0; i < poolCacheQuantity; i++)
                {
                    GameObject poolObj = Instantiate(_poolType, Vector3.zero, Quaternion.identity) as GameObject;
                    poolObj.name = _poolType.name + i.ToString();
                    poolObj.transform.parent = poolContainers[poolContainers.Count - 1].transform;
                    poolObj.SetActive(false);
                    objectPool.Add(poolObj);
                }

                return objectPool;
            }
            return null;
        }

        private List<GameObject> NewPool(GameObject _poolType, Transform newParent)
        {
            if (!poolContainers.Contains(_poolType))
            {
                List<GameObject> objectPool = new List<GameObject>();
                poolContainers.Add(newParent.gameObject);

                for (int i = 0; i < poolCacheQuantity; i++)
                {
                    GameObject poolObj = Instantiate(_poolType, Vector3.zero, Quaternion.identity) as GameObject;
                    poolObj.name = _poolType.name + i.ToString();
                    poolObj.transform.SetParent(poolContainers[poolContainers.Count - 1].transform);
                    poolObj.SetActive(false);
                    objectPool.Add(poolObj);
                }

                return objectPool;
            }
            return null;
        }
    }

}