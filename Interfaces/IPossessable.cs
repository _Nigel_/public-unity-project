﻿using UnityEngine;
using System.Collections;

public interface IPossessable
{
    bool possess { get; set; }
    float possessionCost { get; }
    void UpdateControls();
    void UpdateCamera();
}
