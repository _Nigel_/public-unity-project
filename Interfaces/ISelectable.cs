﻿using UnityEngine;
using System.Collections;
using RTSGame.Units;

public interface ISelectable
{
    int playersTeam { get; }
    bool canMove { get; }
    bool canAttack { get; }
    bool GiveCommand(IUnit obj);
    bool GiveCommand(IDamageable dmgable);
    bool GiveCommand(Vector3 pos);
    IUnit unit { get; }
}