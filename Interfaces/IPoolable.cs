﻿using UnityEngine;
using System.Collections;

namespace RTSGame
{
    public interface IPoolable
    {
        void Activate();
        void Activate(Vector3 position, Vector3 rotation);
        void Deactivate();
    }
}