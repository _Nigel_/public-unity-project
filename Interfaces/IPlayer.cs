﻿using UnityEngine;
using System.Collections.ObjectModel;
using RTSGame.Units;
using RTSGame.Players;

public interface IPlayer
{
    void PlayerSetup(int newPlayerID);
    void LoadUnits(string[] unitList, Vector3 startingPoint);
    void LoadUnit(string unitName);
    void AddTarget(IUnit newTarget);
    void PossessUnit(IPossessable newUnit);
    void UnpossessUnit();
    void LostUnit(IUnit unit);

    ReadOnlyCollection<IUnitController> units { get; }
    ReadOnlyCollection<IUnitController[]> squads { get; }
    ReadOnlyCollection<IUnit> targets { get; }
    ICapacitor capacitor { get; }
    IPlayerController controller { get; }
    IPossessable possessedUnit { get; }
    IPossessable defaultPossessedUnit { get; }
    GameObject obj { get; }
    Color playerColor { get; }
    int team { get; }
    int playerID { get; }
}
