﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;

namespace RTSGame
{
    public class SoundManager : RTSBase
    {
        private static SoundManager _soundManager;
        [SerializeField] private AudioSource musicSource;
        [SerializeField] private int sfxSources;
        [SerializeField, Range(0,1)] private float _sfxVolume, _musicVolume;
        [SerializeField] float lowPitchRange, highPitchRange;
        [SerializeField] float minDistance, maxDistance;
        private AudioSource[] sfxSource;
        
        public float sfxVolume { get { return _sfxVolume; } set { _sfxVolume = value; } }
        public float musicVolume { get { return _musicVolume; } set { _musicVolume = value; _soundManager.musicSource.volume = _musicVolume; } }
        public float randomPitch { get { return Random.Range(lowPitchRange, highPitchRange); } }
        
        public static SoundManager soundManager
        {
            get
            {
                if (_soundManager == null)
                {
                    GameObject obj = Resources.Load("SoundManager") as GameObject;
                    _soundManager = obj.GetComponent<SoundManager>();
                    _soundManager.Initialise();
                }

                return _soundManager;
            }
        }

        public static void PlayClip(AudioClip clip)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.clip = clip;
            freeAudioSource.Play();
        }

        public static void PlayClip(AudioClip clip, bool randomise)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.clip = clip;
            freeAudioSource.Play();
        }

        public static void PlayClip(AudioClip clip, float _minDistance, float _maxDistance, bool randomise)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, _minDistance, _maxDistance);
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.clip = clip;
            freeAudioSource.Play();
        }
        
        public static void RandomClip(params AudioClip[] clips)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.clip = clips[Random.Range(0, clips.Length)];
            freeAudioSource.Play();
        }

        public static void RandomClipAtPoint(Vector3 position, params AudioClip[] clips)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.transform.position = position;
            freeAudioSource.clip = clips[Random.Range(0, clips.Length)];
            freeAudioSource.Play();
        }

        public static void PlayClipAtPoint(AudioClip clip, Vector3 position, float volume)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.transform.position = position;
            freeAudioSource.volume = volume;
            freeAudioSource.clip = clip;
            freeAudioSource.Play();
        }

        public static AudioSource PlayClipAtPoint(AudioClip clip, Vector3 position, float volume, bool looping)
        {
            AudioSource freeAudioSource = soundManager.availableAudioSource;
            soundManager.SetAudioDistance(ref freeAudioSource, soundManager.minDistance, soundManager.maxDistance);
            freeAudioSource.loop = looping;
            freeAudioSource.pitch = soundManager.randomPitch;
            freeAudioSource.transform.position = position;
            freeAudioSource.volume = volume;
            freeAudioSource.clip = clip;
            freeAudioSource.Play();
            return freeAudioSource;
        }

        protected override bool Initialise()
        {
            if (_soundManager == null)
            {
                _soundManager = this;
                DontDestroyOnLoad(gameObject);
            }
            else
            {
                if(_soundManager != this)
                {
                    Destroy(gameObject);
                }
            }
            
            SetupAudioSources();

            return base.Initialise();
        }
        
        private void SetupAudioSources()
        {
            _soundManager.sfxSource = new AudioSource[sfxSources];
            _soundManager.musicSource.volume = _musicVolume;

            for (int i = 0; i < sfxSources; i++)
            {
                GameObject sourceObj = new GameObject("AudioSource");
                sourceObj.transform.parent = transform;
                _soundManager.sfxSource[i] = sourceObj.AddComponent<AudioSource>();
                _soundManager.sfxSource[i].playOnAwake = false;
                _soundManager.sfxSource[i].volume = _sfxVolume;
                _soundManager.sfxSource[i].spatialBlend = 1;
                _soundManager.sfxSource[i].minDistance = minDistance;
                _soundManager.sfxSource[i].maxDistance = maxDistance;
            }
        }
        
        private void SetAudioDistance(ref AudioSource editableSource, float min, float max)
        {
            editableSource.minDistance = minDistance;
            editableSource.maxDistance = maxDistance;
        }

        private AudioSource availableAudioSource
        {
            get
            {
                for(int i=0; i < soundManager.sfxSource.Length; i++)
                {
                    if(!soundManager.sfxSource[i].isPlaying)
                    {
                        return soundManager.sfxSource[i];
                    }
                }

                return soundManager.sfxSource[0];
            }
        }
    }
}