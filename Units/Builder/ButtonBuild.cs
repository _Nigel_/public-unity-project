﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using RTSGame.Players;

namespace RTSGame.Units.UnitBuilder
{
    [RequireComponent(typeof(Button))]
    public class ButtonBuild : RTSBase
    {
        protected override bool Initialise()
        {
            GetComponent<Button>().onClick.AddListener(() =>
            {
                BuildUnitRequest();
            });

            return base.Initialise();
        }

        public void BuildUnitRequest()
        {
            transform.root.GetComponentInChildren<IUnitBuilder>().Build(PlayerControls.player);
        }
    }
}
