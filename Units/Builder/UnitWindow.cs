﻿using UnityEngine;
using System.Collections.ObjectModel;
using UnityEngine.UI;

namespace RTSGame.Units.UnitBuilder
{
    public class UnitWindow : RTSBase
    {
        [SerializeField] UnitPart unitType;
        [SerializeField] private Vector2 sizePerModel;
        ReadOnlyCollection<GameObject> objectList;
        [SerializeField] private ButtonUnitPiece modelButton;
        [SerializeField] private RectTransform container;
        private IUnitBuilder builder;
        
        protected override bool Initialise()
        {
            if(builder == null)
            {
                builder = transform.root.GetComponentInChildren<UnitBuilder>();
            }

            InitialiseMenu();
            return base.Initialise();
        }

        private void InitialiseMenu()
        {
            objectList = builder.RequestUnitPartList(unitType);

            for(int i=0; i < objectList.Count; i++)
            {
                ButtonUnitPiece newButton = Instantiate(modelButton, container.transform.position, container.transform.rotation) as ButtonUnitPiece;
                newButton.transform.SetParent(container);
                newButton.transform.localScale = Vector3.one;
                newButton.index = i;
                newButton.unitPart = objectList[i];
                newButton.part = unitType;
                newButton.GetComponentInChildren<Text>().text = objectList[i].name;
            }

            container.sizeDelta = new Vector2(sizePerModel.x, (sizePerModel.y * objectList.Count));
        }
    }
}