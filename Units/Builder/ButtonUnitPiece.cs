﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RTSGame.Units.UnitBuilder
{
    [RequireComponent(typeof(Button))]
    public class ButtonUnitPiece : RTSBase
    {
        public int index;
        public GameObject unitPart;
        public UnitPart part;
        private Button btn;

        protected override bool Initialise()
        {
            btn = GetComponent<Button>();
            btn.onClick.AddListener(() =>
            {
                SetNewPiece();
            });

            return base.Initialise();
        }

        public void SetNewPiece()
        {
            transform.root.GetComponentInChildren<IUnitBuilder>().Set(part, unitPart);
        }
    }
}
