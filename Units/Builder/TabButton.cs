﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RTSGame.Units.UnitBuilder
{
    public class TabButton : RTSBase
    {
        private IUnitBuilder builder;
        private Button button;
        [SerializeField] private UnitPart tabType;

        protected override bool Initialise()
        {
            builder = transform.GetComponentInParent<IUnitBuilder>();
            builder.onUpdatedUnit += UpdateButtons;

            button = GetComponent<Button>();
            return base.Initialise();
        }

        private void OnEnable()
        {
            if(isInitialised)
            {
                switch(tabType)
                {
                    case UnitPart.Base:
                        {
                            button.interactable = true;
                            break;
                        }
                    case UnitPart.Cockpit:
                        {
                            button.interactable = builder.hasBase;
                            break;
                        }
                    case UnitPart.Extension:
                        {
                            button.interactable = builder.hasBody;
                            break;
                        }
                }
            }
        }

        private void UpdateButtons(UnitPart part)
        {
            switch (tabType)
            {
                case UnitPart.Base:
                    {
                        button.interactable = true;
                        break;
                    }
                case UnitPart.Cockpit:
                    {
                        button.interactable = builder.hasBase;
                        break;
                    }
                case UnitPart.Extension:
                    {
                        button.interactable = builder.hasBody;
                        break;
                    }
            }
        }
    }
}
