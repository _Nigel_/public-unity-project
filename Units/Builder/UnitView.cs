﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.UnitBuilder
{
    public class UnitView : RTSBase
    {
        private Vector3 lastMousePosition;

        protected override bool Initialise()
        {
            return base.Initialise();
        }

        void OnMouseDown()
        {
            lastMousePosition = Input.mousePosition;
        }

        void OnMouseDrag()
        {
            float yaw = lastMousePosition.x - Input.mousePosition.x;

            transform.Rotate(new Vector3(0, yaw, 0));

            lastMousePosition = Input.mousePosition;
        }
    }
}