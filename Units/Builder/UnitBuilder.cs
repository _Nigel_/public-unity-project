﻿using UnityEngine;
using System.Collections.ObjectModel;
using System.Collections.Generic;
using UnityEngine.UI;
using RTSGame.UI;

namespace RTSGame.Units.UnitBuilder
{
    public enum UnitPart { Base, Cockpit, Extension }
    public delegate void UpdatedUnit(UnitPart part);

    public class UnitBuilder : RTSBase, IUnitBuilder
    {
        public event UpdatedUnit onUpdatedUnit;

        public GameObject builderObject { get { if (this) return gameObject; else return null; } }
        public bool hasBase { get { return unitBase != null ? true : false; } }
        public bool hasBody { get { return unitCockpit != null ? true : false; } }

        [SerializeField] private List<GameObject> _unitCockpits;
        [SerializeField] private List<GameObject> _unitBases;
        [SerializeField] private List<GameObject> _unitExtensions;

        private PlayerUI playerUI;
        private GameObject unitCockpit, unitBase, unitWeaponL, unitWeaponR, unitWeaponTop;
        private GameObject[] unitParts;
        [SerializeField] private Transform unit;
        [SerializeField] private int unlistedUnitID;
        
        protected override bool Initialise()
        {
            playerUI = transform.root.GetComponentInChildren<PlayerUI>();

            return base.Initialise();
        }

        public ReadOnlyCollection<GameObject> RequestUnitPartList(UnitPart unitType)
        {
            switch (unitType)
            {
                case UnitPart.Base:
                    {
                        return _unitBases.AsReadOnly();
                    }
                case UnitPart.Cockpit:
                    {
                        return _unitCockpits.AsReadOnly();
                    }
                case UnitPart.Extension:
                    {
                        return _unitExtensions.AsReadOnly();
                    }
                default:
                    {
                        Debug.Log("UnitBuilder::RequestUnitPartList Unrecognised UnitPart!");
                        return default(ReadOnlyCollection<GameObject>);
                    }
            }
        }

        public void Set(UnitPart partType, GameObject newPart)
        {
            switch (partType)
            {
                case UnitPart.Base:
                    {
                        if (unitBase != null)
                        {
                            Destroy(unitBase);
                        }

                        unitBase = Instantiate(newPart, unit.position, Quaternion.identity) as GameObject;
                        unitBase.transform.parent = unit;
                        unitBase.transform.localPosition = Vector3.zero;
                        unitBase.transform.localRotation = Quaternion.Euler(Vector3.zero);

                        break;
                    }
                case UnitPart.Cockpit:
                    {
                        if (unitBase != null)
                        {
                            if (unitCockpit != null)
                            {
                                Destroy(unitCockpit);
                            }

                            unitCockpit = Instantiate(newPart, Vector3.zero, Quaternion.identity) as GameObject;

                            unitCockpit.transform.parent = GetMount(unitBase.transform, "Mount_Top");
                            unitCockpit.transform.localPosition = Vector3.zero;
                            unitCockpit.transform.localScale = Vector3.one;
                            unitCockpit.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        }
                        break;
                    }
                case UnitPart.Extension:
                    {
                        if(unitCockpit != null)
                        {
                            if(unitWeaponL != null)
                            {
                                Destroy(unitWeaponL);
                            }

                            unitWeaponL = Instantiate(newPart, Vector3.zero, Quaternion.identity) as GameObject;

                            unitWeaponL.transform.parent = GetMount(unitBase.transform, "Mount_Weapon_L");
                            unitWeaponL.transform.localPosition = Vector3.zero;
                            unitWeaponL.transform.localScale = Vector3.one;
                            unitWeaponL.transform.localRotation = Quaternion.Euler(Vector3.zero);
                        }

                        break;
                    }
                default:
                    {
                        Debug.Log("UnitBuilder::RequestUnitPartList Unrecognised UnitPart!");
                        break;
                    }
            }

            if (onUpdatedUnit != null)
            {
                onUpdatedUnit(partType);
            }
        }

        private Transform GetMount(Transform part, string search)
        {
            return part.FindDeepChild(search);
        }

        public void Show()
        {
            Time.timeScale = 0;
            gameObject.SetActive(true);
            playerUI.gameObject.SetActive(false);
        }

        public void Hide()
        {
            Time.timeScale = 1;
            playerUI.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        public void Build(IPlayer player)
        {
            unlistedUnitID++;
            UnitFactory.AddUnit(unit.gameObject);
            player.LoadUnit(unit.name);
        }
    }
}