﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units
{
    public delegate void DamageTaken(IUnit iUnit);
    public delegate void Destroyed(IUnit iUnit);
    
    public class Unit : RTSBase, IDamageable, IUnit, IEnergyCost, IExperience
    {
        public event Destroyed Destroyed;
        public event DamageTaken Damaged;
        public event LevelUp LevelUp;
        
        public float creationCost { get { return _creationCost; } }
        public float energyCost { get { return _energyCost; } }
        public float maxHealth { get { return _healthMax; } }
        public float health { get { return _health; } }

        public int team { get { return controller.team; } }
        public int currentExp { get { return _experience; } }
        public int currentLevel { get { return _experienceLevel; } }
        public int worthExperience { get { return 5 + (int)dangerRating + (int)(_experience * 0.1f); } }
        public int tilNextLevel { get { return ExperienceTable.ExperienceRequirement[currentLevel + 1] - currentExp; } }

        public GameObject unitObject { get { if (this) { return gameObject; } else { return null; } } }
        public GameObject damageableObject { get { if ((System.Object)gameObject != null) return gameObject; else return null; } }
        public IUnitController controller { get { if (_controller == null) { _controller = transform.root.GetComponentInChildren<IUnitController>(); } return _controller; } }
        public IExperience experiencer { get { if (this) { return this as IExperience; } else { return null; } } }
        
        public float dangerRating
        {
            get
            {
                IWeapon[] weapons = GetComponentsInChildren<IWeapon>();

                if (weapons != null && weapons.Length > 0)
                {
                    _priorityRating = 0;
                    for (int i = 0; i < weapons.Length; i++)
                    {
                        _priorityRating += weapons[i].dangerRating;
                    }
                }

                return _priorityRating + _baseDangerRating;
            }
        }

        [SerializeField] protected float _priorityRating;
        [SerializeField] protected float _baseDangerRating;
        [SerializeField] protected float _creationCost, _energyCost;
        [SerializeField] protected int _experience = 0;
        [SerializeField] protected int _experienceLevel = 0;
        [SerializeField] protected float _healthMax;
        [SerializeField] protected float _armor;
        [SerializeField] protected Material undamagedMaterial, destroyedMaterial;
        protected IUnitController _controller;
        protected float _health;

        protected override bool Initialise()
        {
            SetupUnit();
            return base.Initialise();
        }

        void OnDestroy()
        {
            LevelUp -= LevelUpAdjustment;
        }

        protected void SetupUnit()
        {
            LevelUp += LevelUpAdjustment;

            IUnitExtension[] extensions = transform.root.GetComponentsInChildren<IUnitExtension>();
            for (int i = 0; i < extensions.Length; i++)
            {
                _healthMax += extensions[i].maxHealth;
                _armor += extensions[i].armor;
            }

            _health = _healthMax;
        }

        public void GiveExperience(int exp)
        {
            _experience += exp;

            if (currentLevel < ExperienceTable.LevelCap && _experience > ExperienceTable.ExperienceRequirement[currentLevel + 1])
            {
                _experienceLevel++;

                if(LevelUp != null)
                {
                    LevelUp(this as IUnit, currentLevel);
                }
            }
        }
        
        public void ManualLevelUp()
        {
            if(currentLevel < ExperienceTable.LevelCap)
            {
                _experienceLevel++;

                if (LevelUp != null)
                {
                    LevelUp(this as IUnit, currentLevel);
                }
            }
        }
        
        public void LevelUpAdjustment(IUnit unit, int newlevel)
        {
            _healthMax += 50;
            _armor += 2;
            _baseDangerRating += 0.5f;
        }
        
        public virtual bool TakeDamage(IUnit damager, float dmg, float armrPen)
        {
            bool wasDamaged = false;

            if (isInitialised && _health > 0)
            {
                float remainingArmor = Mathf.Clamp((_armor - armrPen), 0, Mathf.Infinity);
                float trueDamage = Mathf.Clamp((dmg - remainingArmor), 0, Mathf.Infinity);

                if(trueDamage > 0 )
                {
                    wasDamaged = true;
                    _health -= trueDamage;
                    
                    if (Damaged != null)
                    {
                        Damaged(this as IUnit);
                    }

                    if (_health <= 0)
                    {
                        IExperience experiencer = damager.unitObject.GetComponent<IExperience>();
                        if (experiencer != null)
                        {
                            experiencer.GiveExperience(worthExperience);
                        }

                        UnitDestroyed();
                    }
                }
            }

            return wasDamaged;
        }

        public virtual bool TakeDamage(IUnit damager, float dmg)
        {
            bool wasDamaged = false;

            if (isInitialised && _health > 0)
            {
                float trueDamage = Mathf.Clamp((dmg - _armor), 0, Mathf.Infinity);

                if (trueDamage > 0)
                {
                    wasDamaged = true;
                    _health -= trueDamage;

                    if (Damaged != null)
                    {
                        Damaged(this as IUnit);
                    }

                    if (_health <= 0)
                    {
                        IExperience experiencer = damager.unitObject.GetComponent<IExperience>();
                        if(experiencer != null)
                        {
                            experiencer.GiveExperience(worthExperience);
                        }

                        UnitDestroyed();
                    }
                }
            }

            return wasDamaged;
        }

        public virtual void UnitDestroyed()
        {
            MeshRenderer[] renderers = transform.root.GetComponentsInChildren<MeshRenderer>();
            for(int i=0; i < renderers.Length; i++)
            {
                renderers[i].material = destroyedMaterial;
            }

            if (Destroyed != null)
            {
                Destroyed(this as IUnit);
            }
        }
    }
}