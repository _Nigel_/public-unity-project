﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public static class ExperienceTable
    {
        public const int LevelCap = 5;
        public static readonly int[] ExperienceRequirement = { 0, 100, 250, 500, 1000, 2500 };
    }
}
