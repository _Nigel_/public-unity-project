﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.AI
{
    public class Command : ICommand
    {
        public event OnStatusChanged onStatusChanged;
        public event OnBecameExecutable onCommandAccepted;

        private readonly float protectRange = 10;
        private CommandType command;
        private CommandStatus _status = CommandStatus.Inactive;
        private IUnitController controller;
        private GameObject target;
        private Vector3 targetPosition;
        private bool _isComplete = false;

        public CommandStatus status { get { return _status; } }
        public CommandType commandType { get { return command; } }
        public bool isComplete { get { return _isComplete; } }

        public Command(IUnitController unitController, GameObject targetObjective, CommandType newCommandType)
        {
            if(targetObjective != null && unitController != null)
            {
                target = targetObjective;
                command = newCommandType;
                controller = unitController;
                ChangeStatus(CommandStatus.Pending);

                if (controller.transporter != null)
                {
                    controller.transporter.onPathFound += AcceptedMove;
                    controller.transporter.onReachedDestination += CompletedMove;
                    controller.transporter.Move(targetObjective.transform.position, controller.weapons[0].weaponRange);
                }
            }
        }
        
        public Command(IUnitController unitController, IUnit targetObjective, CommandType newCommandType)
        {
            if (targetObjective != null && unitController != null)
            {
                target = targetObjective.unitObject;
                command = newCommandType;
                controller = unitController;
                ChangeStatus(CommandStatus.Pending);

                if (controller.transporter != null)
                {
                    controller.transporter.onPathFound += AcceptedMove;
                    controller.transporter.onReachedDestination += CompletedMove;
                    controller.transporter.Move(targetObjective.unitObject.transform.position, controller.weapons[0].weaponRange);
                }
            }
        }

        public Command(IUnitController unitController, IDamageable targetObjective, CommandType newCommandType)
        {
            if (targetObjective != null && unitController != null)
            {
                target = targetObjective.damageableObject;
                command = newCommandType;
                controller = unitController;
                ChangeStatus(CommandStatus.Pending);

                if (controller.transporter != null)
                {
                    controller.transporter.onPathFound += AcceptedMove;
                    controller.transporter.onReachedDestination += CompletedMove;
                    controller.transporter.Move(targetObjective.damageableObject.transform.position, controller.weapons[0].weaponRange);
                }
            }
        }

        public Command(IUnitController unitController, Vector3 targetLocation, CommandType newCommandType)
        {
            if (targetLocation != default(Vector3) && unitController != null)
            {
                command = newCommandType;
                targetPosition = targetLocation;
                controller = unitController;
                ChangeStatus(CommandStatus.Pending);

                if(controller.transporter != null)
                {
                    controller.transporter.onPathFound += AcceptedMove;
                    controller.transporter.onReachedDestination += CompletedMove;
                    controller.transporter.Move(targetLocation);
                }
            }
        }

        public void AcceptedMove(IUnit unit, Vector3[] path)
        {
            if (onCommandAccepted != null)
            {
                onCommandAccepted(this, path);
            }

            controller.transporter.onPathFound -= AcceptedMove;
        }

        public void CompletedMove()
        {
            Debug.Log("Completed Move");
            ChangeStatus(CommandStatus.Completed);
        }
        
        public void ActionCommand()
        {
            switch(command)
            {
                case CommandType.Attack:
                    {
                        if(target != null)
                        {
                            if (Vector3.Distance(controller.transporter.destination, target.transform.position) > 5)
                            {
                                //controller.transporter.Move(target.transform.position, controller.weapons[0].weaponRange);
                            }
                        }
                        break;
                    }
                case CommandType.Move:
                    {
                        if (Vector3.Distance(controller.transporter.destination, targetPosition) > 5)
                        {
                            controller.transporter.Move(targetPosition);
                        }
                        break;
                    }
                case CommandType.Guard:
                    {
                        controller.transporter.Move(target.transform.position, protectRange);
                        break;
                    }
                default:
                    {
                        #if UNITY_EDITOR
                        Debug.Log("Command::ActionCommand - Unrecognised CommandType!");
                        #endif

                        break;
                    }
            }
        }

        public void Complete()
        {
            _isComplete = true;
        }

        public void ChangeStatus(CommandStatus updatedStatus)
        {
            _status = updatedStatus;

            if(onStatusChanged != null)
            {
                onStatusChanged(this, _status);
            }
        }
    }
}