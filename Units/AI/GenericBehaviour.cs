﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units.AI
{
    public class GenericBehaviour : RTSBase, IUnitBehaviour
    {
        private IUnitController controller;
        private Vector3 originalPosition;
        private float optimumWeaponRange;
        private bool moving = false, engaged = false;
        
        protected override bool Initialise()
        {
            IWeapon[] weapons = transform.root.GetComponentsInChildren<IWeapon>();

            if (weapons.Length > 0)
            {
                optimumWeaponRange = weapons[0].weaponRange;

                for (int i = 0; i < weapons.Length; i++)
                {
                    if(weapons[i].weaponRange < optimumWeaponRange)
                    {
                        optimumWeaponRange = weapons[i].weaponRange;
                    }
                }
            }
            
            if (controller == null)
            {
                controller = transform.root.GetComponentInChildren<IUnitController>();
            }

            return base.Initialise();
        }
        
        public void BehaviourAction()
        {
            if (!isInitialised)
            {
                Initialise();
            }

        }
    }
}