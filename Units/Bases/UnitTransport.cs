﻿using UnityEngine;
using System.Collections;
using Pathfinding;

namespace RTSGame.Units
{
    public delegate void OnPathFound(IUnit unit, Vector3[] path);
    public delegate void OnReachedDestination();
    
    [RequireComponent(typeof(Seeker))]
    public class UnitTransport : UnitExtension, ITransport
    {
        public event OnPathFound onPathFound;
        public event OnReachedDestination onReachedDestination;

        public Vector3 destination { get; protected set; }
        public GameObject target;
        
        protected int currentWaypoint = 0;
        protected Path path;
        protected Seeker seeker;
        protected Rigidbody rigidBody;
        protected bool isMoving = false, _manualControl = false;
        protected float nextWaypointDistance = 1;
        [SerializeField] protected float turnSpeed, thrust, strafe;
        
        public virtual bool manualControl
        {
            get
            {
                return _manualControl;
            }
            set
            {
                _manualControl = value;

                if (_manualControl)
                {
                    isMoving = false;
                    rigidBody.isKinematic = false;
                }
                else
                {
                    rigidBody.isKinematic = true;
                }
            }
        }

        public override void Activate()
        {
            if(seeker == null)
            {
                seeker = GetComponent<Seeker>();
            }

            if (rigidBody == null)
            {
                rigidBody = transform.root.GetComponentInChildren<Rigidbody>();
            }

            rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;

            base.Activate();
        }

        public override void Deactivate()
        {
            isMoving = false;
            rigidBody.isKinematic = false;
            rigidBody.constraints = RigidbodyConstraints.None;
            rigidBody.useGravity = true;
        }

        public virtual Vector3 Velocity()
        {
            return rigidBody.velocity;
        }

        public virtual void Move(Vector3 position, float desiredRange)
        {
            nextWaypointDistance = desiredRange;
            destination = position;

            isMoving = false;
            seeker.StartPath(transform.position, position, OnPathComplete);
        }

        public virtual void Move(Vector3 position)
        {
            destination = position;

            isMoving = false;
            seeker.StartPath(transform.position, position, OnPathComplete);
        }

        public virtual void Face(Transform target)
        {
            rigidBody.transform.LookAt(target);
        }

        public virtual void ManualMove(Vector3 moveAmount)
        {
            rigidBody.AddRelativeForce(new Vector3(moveAmount.y * strafe, 0, moveAmount.z * thrust), ForceMode.Acceleration);
            rigidBody.AddRelativeTorque(new Vector3(0, moveAmount.x * turnSpeed, 0), ForceMode.Acceleration);
            rigidBody.angularVelocity = Vector3.ClampMagnitude(rigidBody.angularVelocity, turnSpeed);
        }

        public virtual void OnPathComplete(Path p)
        {
            if (!p.error)
            {
                path = p;
                currentWaypoint = 0;
                OnPathFound(path.vectorPath.ToArray());

                if (!isMoving)
                {
                    StartCoroutine(CycleMove());
                }
            }
        }

        protected virtual IEnumerator CycleMove()
        {
            isMoving = true;

            while (path != null && isMoving)
            {
                if (currentWaypoint >= path.vectorPath.Count)
                {
                    isMoving = false;
                    continue;
                }

                Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                dir *= thrust * Time.deltaTime;
                rigidBody.AddForce(dir * 100, ForceMode.Acceleration);
                
                Vector3 dot = rigidBody.transform.InverseTransformDirection((path.vectorPath[currentWaypoint] - transform.position).normalized);
                rigidBody.AddRelativeTorque(new Vector3(0, dot.x, 0) * 50, ForceMode.Acceleration);
                
                if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
                {
                    currentWaypoint++;
                }

                yield return new WaitForFixedUpdate();
            }

            if (onReachedDestination != null)
            {
                onReachedDestination();
            }
            
        }

        protected virtual void OnReachedDestination()
        {
            nextWaypointDistance = 1;
            if (onReachedDestination != null)
            {
                onReachedDestination();
            }
        }

        protected void OnPathFound(Vector3[] path)
        {
            if(onPathFound != null)
            {
                onPathFound(this as IUnit, path);
            }
        }

        protected override void LevelUp(IUnit unit, int newLevel)
        {

        }
    }
}