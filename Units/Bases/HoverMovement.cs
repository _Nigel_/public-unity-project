﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public class HoverMovement : UnitTransport
    {
        [SerializeField] float maxSpeed, maxTurn, optimumHeight;
        [SerializeField] float upwardThrust;
        [SerializeField] float acceleration;
        [SerializeField] LayerMask layerMask;
        private float offset;

        public override void Activate()
        {
            base.Activate();

            if (rigidBody != null)
            {
                rigidBody.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
                rigidBody.isKinematic = false;
                rigidBody.useGravity = true;
                rigidBody.mass = 1;
                rigidBody.drag = 0.5f;
                rigidBody.angularDrag = 5f;
            }

            //RaycastHit hit;
            //Ray ray = new Ray(transform.position, Vector3.down * 500);

            //if (Physics.Raycast(ray, out hit))
            //{
            //    transform.root.position = hit.point + (Vector3.up * optimumHeight);
            //}
        }

        public override void Deactivate()
        {
            isMoving = false;
            rigidBody.isKinematic = false;
            rigidBody.constraints = RigidbodyConstraints.None;
            rigidBody.useGravity = true;
            rigidBody.angularDrag = 0.01f;
            rigidBody.AddTorque(rigidBody.velocity.normalized * 10);
        }

        protected override IEnumerator CycleMove()
        {
            isMoving = true;

            while (isMoving)
            {
                Vector3 dot = rigidBody.transform.InverseTransformDirection((path.vectorPath[currentWaypoint] - transform.position).normalized);

                if (Mathf.Abs(thrust) > 0 && rigidBody.velocity.magnitude < maxSpeed)
                {
                    rigidBody.AddForce(transform.forward * thrust, ForceMode.Acceleration);
                }
                if (Mathf.Abs(turnSpeed) > 0 && rigidBody.angularVelocity.magnitude < maxTurn)
                {
                    rigidBody.AddRelativeTorque(new Vector3(0, dot.x, 0) * turnSpeed, ForceMode.Acceleration);
                }

                RaycastHit hit;
                Ray ray = new Ray(transform.position, Vector3.down);
                if (Physics.Raycast(ray, out hit, 500, layerMask))
                {
                    float diff = Mathf.Abs(hit.distance - optimumHeight);
                    if (hit.distance < optimumHeight)
                    {
                        rigidBody.AddForce(Vector3.up * 1 * Mathf.Clamp(diff, 0, 10), ForceMode.Acceleration);
                    }
                    else
                    {
                        rigidBody.AddForce(Vector3.down * Mathf.Clamp(diff, 0, 10), ForceMode.Acceleration);
                    }
                }

                yield return new WaitForFixedUpdate();
            }

            OnReachedDestination();
        }
    }
}