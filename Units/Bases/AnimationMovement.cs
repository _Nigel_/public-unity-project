﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public class AnimationMovement : UnitTransport, ITransport
    {
        private Vector2 velocity = Vector2.zero;
        [SerializeField] protected float maxForward, maxTurn;
        Animator animator;

        public override bool manualControl
        {
            get
            {
                return _manualControl;
            }
            set
            {
                _manualControl = value;

                if (_manualControl)
                {
                    isMoving = false;
                }
                else
                {

                }
            }
        }

        public override void Activate()
        {
            base.Activate();

            if(animator == null)
            {
                animator = GetComponent<Animator>();
            }
        }

        public override void Deactivate()
        {
            isMoving = false;
            rigidBody.isKinematic = false;
            animator.SetTrigger("Died");

            base.Deactivate();
        }

        public override void ManualMove(Vector3 moveAmount)
        {
            rigidBody.AddRelativeForce(new Vector3(moveAmount.y * strafe, 0, moveAmount.z * thrust), ForceMode.Acceleration);
            rigidBody.AddRelativeTorque(new Vector3(0, moveAmount.x * turnSpeed, 0), ForceMode.Acceleration);
            rigidBody.angularVelocity = Vector3.ClampMagnitude(rigidBody.angularVelocity, turnSpeed);
        }

        protected override IEnumerator CycleMove()
        {
            isMoving = true;
            animator.SetBool("moving", true);

            while (path != null && isMoving)
            {
                if (currentWaypoint >= path.vectorPath.Count)
                {
                    isMoving = false;
                }
                else
                {
                    Vector3 dir = (path.vectorPath[currentWaypoint] - transform.position).normalized;
                    dir *= thrust * Time.deltaTime;

                    velocity.x = Mathf.Clamp(velocity.x + 0.5f, 0, maxForward);
                    animator.SetFloat("vely", velocity.x);

                    transform.LookAt(path.vectorPath[currentWaypoint]);

                    if (Vector3.Distance(transform.position, path.vectorPath[currentWaypoint]) < nextWaypointDistance)
                    {
                        currentWaypoint++;
                    }
                }
                
                yield return new WaitForFixedUpdate();
            }

            while(velocity.x > 0.1f)
            {
                velocity.x = Mathf.Clamp(velocity.x - 0.1f, 0, maxForward);
                animator.SetFloat("vely", velocity.x);
            }

            animator.SetFloat("vely", 0);
            animator.SetBool("moving", false);
            OnReachedDestination();
        }

        protected override void LevelUp(IUnit unit, int newLevel)
        {

        }
    }
}
