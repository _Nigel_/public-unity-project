﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units
{
    public class SocketableObject : RTSBase, ISocketable
    {
        public bool socketed { get { return _socketed; } }

        [SerializeField]
        private LayerMask layerMask;
        [SerializeField]
        private Material socketableMaterial;
        [SerializeField]
        private Color socketableColor, unsocketableColor;
        private MeshRenderer meshRenderer;
        private Material originalMaterial;
        private Vector3 originalPosition;
        private Quaternion originalRotation;
        private bool _socketed = false;

        protected override bool Initialise()
        {
            meshRenderer = GetComponent<MeshRenderer>();
            originalMaterial = meshRenderer.material;
            originalRotation = transform.rotation;
            return base.Initialise();
        }

        public void Socket(ISocket _socket)
        {
            IUnitController controller = transform.root.GetComponentInChildren<IUnitController>();

            if (controller != null)
            {
                object extensionType = (object)GetComponent<ISensor>();

                if (extensionType == null)
                {
                    extensionType = (object)GetComponent<IWeapon>();
                }

                controller.AddExtension((object)extensionType);
            }

            _socketed = true;
            this.enabled = false;
        }

        void OnMouseDown()
        {
            if (!_socketed)
            {
                originalPosition = transform.position;
                meshRenderer.material = socketableMaterial;
            }
        }

        void OnMouseDrag()
        {
            if (!_socketed)
            {
                Ray vRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(vRay, out hit, 1000, layerMask))
                {
                    ISocket socket = hit.collider.GetComponent<ISocket>();

                    if (socket != null && socket.socketAvailable)
                    {
                        socketableMaterial.color = socketableColor;
                        socket.PreviewSocket(gameObject);
                    }
                    else
                    {
                        socketableMaterial.color = unsocketableColor;
                        transform.position = hit.point;
                        transform.rotation = originalRotation;
                    }
                }
            }
        }

        void OnMouseUp()
        {
            if (!_socketed)
            {
                meshRenderer.material = originalMaterial;

                Ray vRay = Camera.main.ScreenPointToRay(Input.mousePosition);
                RaycastHit hit;

                if (Physics.Raycast(vRay, out hit, 1000, layerMask))
                {
                    ISocket socket = hit.collider.GetComponent<ISocket>();
                    if (socket != null)
                    {
                        socket.SetSocket(gameObject);
                    }
                    else
                    {
                        transform.position = originalPosition;
                        transform.rotation = originalRotation;
                    }
                }
            }
        }
    }
}