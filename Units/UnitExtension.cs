﻿#pragma warning disable 0649
using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public abstract class UnitExtension : RTSBase, IEnergyCost, IActivate, IUnitExtension
    {
        [SerializeField] protected float _health;
        [SerializeField] protected float _armor;
        [SerializeField, Tooltip("Passed to main Unit Class and accumulated into a final number, alongside other extensions.")] private int _baseExpToLevel;
        [SerializeField] protected float _creationCost, _energyCost;
        [SerializeField, Tooltip("Tells extension to start it's own actions when the game starts. Extensions attached to Units, or base prefabs, do not need this ticked.")]
        protected bool _activate = false;
        protected IUnit unit;

        public bool activated { get { return _activate; } }
        public float creationCost { get { return _creationCost; } }
        public float energyCost { get { return _energyCost; } }
        public float maxHealth { get { return _health; } }
        public float armor { get { return _armor; } }
        public int baseExp { get { return _baseExpToLevel; } }

        public virtual void Deactivate()
        {
            unit.experiencer.LevelUp -= LevelUp;
        }

        public virtual void Activate()
        {
            if (unit == null)
            {
                unit = transform.root.GetComponentInChildren<IUnit>();
            }
            
            unit.experiencer.LevelUp += LevelUp;
        }

        protected abstract void LevelUp(IUnit unit, int newLevel);
    }
}