﻿#pragma warning disable 0649

using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Linq;
using RTSGame.Players;
using BehaviorDesigner.Runtime;

namespace RTSGame.Units.AI
{
    [System.Serializable]
    public class UnitController : RTSBase, IUnitController, IPossessable, IActivate
    {
        [SerializeField] private bool _activeController = true;
        [SerializeField] protected bool isPossessed = false;
        [SerializeField] private Vector3 possessionCameraOffset;
        [SerializeField] private float possessionCameraZoomScaler = 2.5f;
        [SerializeField] private float minZoom, maxZoom;
        [SerializeField] private BehaviorTree defaultBehavior;

        private Vector3 possessionCameraVelocity = Vector3.zero;
        private Vector3 startDragPosition = Vector3.zero;

        private List<IUnit> _targets = new List<IUnit>();
        private List<IUnit> _friendlies = new List<IUnit>();
        private List<IWeapon> _weapons = new List<IWeapon>();
        private Queue<ICommand> _commands = new Queue<ICommand>();

        public IPlayer owner { get; private set; }
        public IUnit unit { get; private set; }
        public ITransport transporter { get; private set; }
        public ReadOnlyCollection<IUnit> enemies { get { return _targets.AsReadOnly(); } }
        public ReadOnlyCollection<IUnit> friendlies { get { return _friendlies.AsReadOnly(); } }
        public ReadOnlyCollection<IWeapon> weapons { get { return _weapons.AsReadOnly(); } }
        public float possessionCost { get; private set; }
        public int team { get; private set; }

        public bool activeController
        {
            get
            {
                return _activeController;
            }
            set
            {
                _activeController = value;
                if (_activeController)
                {
                    Activate();
                }
                else
                {
                    Deactivate();
                }
            }
        }
        
        public bool possess
        {
            get
            {
                return isPossessed;
            }
            set
            {
                isPossessed = value;

                if(isPossessed)
                {
                    if(transporter != null)
                    {
                        transporter.manualControl = true;
                    }
                }
                else
                {
                    if (transporter != null)
                    {
                        transporter.manualControl = false;
                    }
                }
            }
        }

        public void UpdateControls()
        {
            if(this)
            {
                if (transporter != null)
                {
                    float forward = Input.GetAxis("Vertical");
                    float horizontal = Input.GetAxis("Horizontal");
                    float height = Input.GetAxis("Height");
                    float vertical = Input.GetAxis("Mouse ScrollWheel");

                    possessionCameraOffset = possessionCameraOffset.normalized * ( Mathf.Clamp((possessionCameraOffset.magnitude + (vertical * possessionCameraZoomScaler)), minZoom, maxZoom) );

                    if (Input.GetMouseButtonDown(1))
                    {
                        startDragPosition = Input.mousePosition;
                    }
                    if (Input.GetMouseButton(1))
                    {
                        float differenceY = Input.mousePosition.y - startDragPosition.y;
                        float differenceX = Input.mousePosition.x - startDragPosition.x;
                        startDragPosition = Input.mousePosition;

                        owner.obj.transform.RotateAround(transform.position, owner.obj.transform.right, differenceY);
                        owner.obj.transform.RotateAround(transform.position, transform.up, differenceX);
                    }

                    transporter.ManualMove(new Vector3(horizontal, height, forward));
                }
            }
        }

        public void UpdateCamera()
        {
            Quaternion lookDirection = Quaternion.LookRotation(transform.position - owner.obj.transform.position);
            owner.obj.transform.rotation = Quaternion.Slerp(owner.obj.transform.rotation, lookDirection, Time.time * 0.01f);
            
            if (!PlayerControls.isDraggingRight)
            {
                Vector3 newPosition = Vector3.SmoothDamp(owner.obj.transform.position, transform.TransformPoint(possessionCameraOffset), ref possessionCameraVelocity, 0.35f);
                owner.obj.transform.position = newPosition;
            }
        }

        public void AddTarget(IUnit newTarget)
        {
            if(!_targets.Contains(newTarget))
            {
                owner.AddTarget(newTarget);

                _targets.Add(newTarget);

                newTarget.Destroyed += RemoveTarget;
            }
        }
        
        public void RemoveTarget(IUnit target)
        {
            if (_targets.Contains(target))
            {
                _targets.Remove(target);
                target.Destroyed -= RemoveTarget;
            }
        }

        public void AddFriendly(IUnit newFriendly)
        {
            if (!_friendlies.Contains(newFriendly))
            {
                _friendlies.Add(newFriendly);
                newFriendly.Destroyed += RemoveFriendly;
            }
        }
        
        public void RemoveFriendly(IUnit friendly)
        {
            if (_friendlies.Contains(friendly))
            {
                _friendlies.Remove(friendly);
                friendly.Destroyed -= RemoveFriendly;
            }
        }

        protected override bool Initialise()
        {
            if(unit == null)
            {
                unit = transform.root.GetComponentInChildren<IUnit>();
            }

            if(activeController)
            {
                Activate();
            }

            return base.Initialise();
        }

        public void PreDestructActions(IUnit unitDestroying)
        {
            if(possess)
            {
                owner.UnpossessUnit();
            }

            Deactivate();
        }

        public void SetOwner(IPlayer newOwner)
        {
            owner = newOwner;
        }

        public void SetTeam(int newTeam)
        {
            team = newTeam;
        }

        public void AddExtension(object objType)
        {
            var objShooter = objType as IWeapon;
            
            if (objShooter != null)
            {
                IWeapon weapon = objType as IWeapon;
                _weapons.Add(weapon);
                weapon.weaponEnabled = true;
            }
            else
            {
                #if UNITY_EDITOR
                Debug.Log(string.Format("IController::AddExtension - unrecognised type parsed : {0}", objType.GetType().ToString()));
                #endif
            }
        }
        
        public void Activate()
        {
            _activeController = true;
            EnableWeapons();
            ActivateComponents();

            if (unit != null)
            {
                unit.Destroyed += PreDestructActions;
            }

#if UNITY_EDITOR
            else
            {
                Debug.Log("Did not subscribe to destroy event");
            }
#endif

            transporter = GetComponentInChildren<ITransport>();
            StartCoroutine(ActionArtificialIntelligence());
        }

        public void ActivateComponents()
        {
            IActivate[] activateableList = transform.root.GetComponentsInChildren<IActivate>();
            for (int i = 0; i < activateableList.Length; i++)
            {
                if (activateableList[i] != (System.Object)this)
                {
                    activateableList[i].Activate();
                }
            }
        }

        public void EnergyConsumption()
        {
            if (owner != null)
            {
                IEnergyCost[] energyCosts = transform.root.GetComponentsInChildren<IEnergyCost>();
                float totalCost = 0;
                for (int i = 0; i < energyCosts.Length; i++)
                {
                    totalCost += energyCosts[i].energyCost;
                }
                if (totalCost != 0)
                {
                    owner.capacitor.EnergyOutput(totalCost);
                }
            }
        }

        public void Deactivate()
        {
            _activeController = false;

            IActivate[] activateableList = transform.root.GetComponentsInChildren<IActivate>();
            for (int i = 0; i < activateableList.Length; i++)
            {
                if (activateableList[i] != (System.Object)this)
                {
                    activateableList[i].Deactivate();
                }
            }

            if (unit != null)
            {
                unit.Destroyed -= PreDestructActions;
            }
            else
            {
                Debug.Log("Did not subscribe to destroy event");
            }

            if (owner != null)
            {
                IEnergyCost[] energyCosts = transform.root.GetComponentsInChildren<IEnergyCost>();
                float totalCost = 0;
                for (int i = 0; i < energyCosts.Length; i++)
                {
                    totalCost += energyCosts[i].energyCost;
                }
                if (totalCost != 0)
                {
                    owner.capacitor.EnergyOutput(-totalCost);
                }
            }
        }

        public void EnableWeapons()
        {
            _weapons.AddRange(GetComponentsInChildren<IWeapon>());
            for (int i = 0; i < _weapons.Count; i++)
            {
                if (_weapons[i].weaponEnabled == false)
                {
                    _weapons[i].weaponEnabled = true;
                }
            }
        }
        
        public ICommand NewCommand(IUnit target)
        {
            Command command;
            if (target.team != owner.team)
            {
                command = new Command(this, target, CommandType.Attack);
                AddTarget(target);
            }
            else
            {
                command = new Command(this, target, CommandType.Follow);
                AddFriendly(target);
            }

            _commands.Clear();
            
            command.onStatusChanged += UpdateCommandStatus;
            _commands.Enqueue(command);

            return command;
        }

        public ICommand NewCommand(Vector3 position)
        {
            _commands.Clear();
            if (transporter != null)
            {
                Command command = new Command(this, position, CommandType.Move);
                command.onStatusChanged += UpdateCommandStatus;
                _commands.Enqueue(command);
                return command;
            }
            return null;
        }

        public ICommand NewCommand(IDamageable dmgable)
        {
            _commands.Clear();

            Command command = new Command(this, dmgable, CommandType.Attack);
            command.onStatusChanged += UpdateCommandStatus;
            _commands.Enqueue(command);
            return command;
        }

        public void UpdateCommandStatus(ICommand iCommand, CommandStatus newStatus)
        {
            if (newStatus == CommandStatus.Inactive || newStatus == CommandStatus.Cancelled || newStatus == CommandStatus.Completed)
            {
                if (_commands.Count > 0 && _commands.Peek().Equals(iCommand))
                {
                    _commands.Dequeue();
                }
            }
        }

        private IEnumerator ActionArtificialIntelligence()
        {
            while (_activeController)
            {
                ManageTargets();

                if (!isPossessed)
                {
                    if (_commands.Count > 0)
                    {
                        CommandStatus status = _commands.Peek().status;
                        if (status == CommandStatus.Active || status == CommandStatus.Pending || status == CommandStatus.Accepted)
                        {
                            _commands.Peek().ActionCommand();
                        }
                    }
                    else
                    {
                        BehaviorManager.instance.Tick(defaultBehavior);
                    }
                }
                yield return new WaitForSeconds(RTSUniversalSettings.timeStep);
            }
            
            #if UNITY_EDITOR
            Debug.Log(string.Format("AIController::ActionArtificialIntelligence - Ended on GameObject {0}", gameObject.name));
            #endif
        }

        private void ManageTargets()
        {
            for (int i = 0; i < enemies.Count; i++)
            {
                if (enemies[i].health <= 0)
                {
                    RemoveTarget(enemies[i]);
                }
            }
            
            _targets = _targets.OrderBy( x => Vector3.Distance(this.transform.position, x.unitObject.transform.position)).ToList();
        }
    }
}
