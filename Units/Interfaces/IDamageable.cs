﻿using UnityEngine;
using System.Collections;
using RTSGame.Units;

public interface IDamageable
{
    bool TakeDamage(IUnit damager, float dmg);
    bool TakeDamage(IUnit damager, float dmg, float amrPen);
    GameObject damageableObject { get; }
}