﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units
{
    public interface ISensor
    {
        void SenseAction(ref List<GameObject> targets);
    }
}
