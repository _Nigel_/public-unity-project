﻿using UnityEngine;
using System.Collections;

public interface ISocketable
{
    bool socketed { get; }
    void Socket(ISocket _socket);
}
