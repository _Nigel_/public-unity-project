﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public interface ITransport
    {
        event OnPathFound onPathFound;
        event OnReachedDestination onReachedDestination;

        Vector3 destination { get; }
        bool manualControl { get; set; }

        Vector3 Velocity();
        void Move(Vector3 position);
        void Move(Vector3 position, float desiredRange);
        void ManualMove(Vector3 moveAmount);
        void Face(Transform target);
    }
}