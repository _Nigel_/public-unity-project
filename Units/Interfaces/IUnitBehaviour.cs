﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units
{
    public interface IUnitBehaviour
    {
        void BehaviourAction();
    }
}