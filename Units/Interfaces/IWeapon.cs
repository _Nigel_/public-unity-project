﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public interface IWeapon
    {
        float weaponRange { get; }
        float weaponDamage { get; }
        float weaponFireRate { get; }
        bool weaponEnabled { get; set; }
        float dangerRating { get; }
    }
}