﻿using UnityEngine;
using System.Collections;
using System.Collections.ObjectModel;
using System;

namespace RTSGame.Units
{
    public interface IUnitController
    {
        int team { get; }
        IPlayer owner { get; }
        IUnit unit { get; }
        ITransport transporter { get; }

        bool activeController { get; set; }
        void SetOwner(IPlayer newPlayerController);
        void SetTeam(int newTeam);
        void AddTarget(IUnit newTarget);
        void AddFriendly(IUnit newFriendly);
        void RemoveTarget(IUnit target);
        void RemoveFriendly(IUnit target);
        void AddExtension(object type);
        ReadOnlyCollection<IUnit> enemies { get; }
        ReadOnlyCollection<IUnit> friendlies { get; }
        ReadOnlyCollection<IWeapon> weapons { get; }
        ICommand NewCommand(IUnit target);
        ICommand NewCommand(IDamageable dmgableObj);
        ICommand NewCommand(Vector3 position);
    }
}