﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public delegate void LevelUp(IUnit unit, int newLevel);
    
    public interface IExperience
    {
        event LevelUp LevelUp;

        int currentExp { get; }
        int currentLevel { get; }
        int tilNextLevel { get; }
        int worthExperience { get; }
        
        void GiveExperience(int exp);
    }
}
