﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public interface IUnitExtension
    {
        float maxHealth { get; }
        float armor { get; }
        int baseExp { get; }
    }
}
