﻿using UnityEngine;
using System.Collections;

public interface ISocket
{
    bool socketAvailable { get; }
    bool PreviewSocket(GameObject previewObject);
    bool SetSocket(GameObject setObject);
}
