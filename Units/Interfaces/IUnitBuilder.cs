﻿using UnityEngine;
using System.Collections.ObjectModel;

namespace RTSGame.Units.UnitBuilder
{
    public interface IUnitBuilder
    {
        event UpdatedUnit onUpdatedUnit;

        bool hasBase { get; }
        bool hasBody { get; }
        GameObject builderObject { get; }
        ReadOnlyCollection<GameObject> RequestUnitPartList(UnitPart unitType);
        void Set(UnitPart partType, GameObject newPiece);
        void Show();
        void Hide();
        void Build(IPlayer player);
    }
}
