﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units
{
    public enum CommandType { Move, Guard, Attack, Follow }
    public enum CommandStatus { Inactive, Pending, Accepted, Active, Completed, Cancelled }
    public delegate void OnStatusChanged(ICommand iCommand, CommandStatus newStatus);
    public delegate void OnBecameExecutable(ICommand iCommand, Vector3[] path);

    public interface ICommand
    {
        event OnBecameExecutable onCommandAccepted;
        event OnStatusChanged onStatusChanged;
        CommandStatus status { get; }
        CommandType commandType { get; }
        void ActionCommand();
    }
}
