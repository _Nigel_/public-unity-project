﻿using UnityEngine;
using System.Collections;

public interface IActivate
{
    void Activate();
    void Deactivate();
}
