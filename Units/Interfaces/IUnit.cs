﻿using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units
{
    public interface IUnit
    {
        event DamageTaken Damaged;
        event Destroyed Destroyed;
        
        IUnitController controller { get; }
        IExperience experiencer { get; }
        GameObject unitObject { get; }
        int team { get; }

        float dangerRating { get; }
        float maxHealth { get; }
        float health { get; }
    }
}