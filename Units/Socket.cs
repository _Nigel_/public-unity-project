﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class Socket : MonoBehaviour, ISocket
{
    private bool _socketAvailable = true;
    public bool socketAvailable { get { return true; } }

    public bool PreviewSocket(GameObject previewedObj)
    {
        ISocketable previewableObj = previewedObj.GetComponent<ISocketable>();

        if (_socketAvailable && previewableObj != null)
        {
            previewedObj.transform.position = transform.position;
            previewedObj.transform.rotation = Quaternion.Euler(transform.forward);
            return true;
        }
        else
        {
            return false;
        }
    }

    public bool SetSocket(GameObject setObj)
    {
        ISocketable socketableObj = setObj.GetComponent<ISocketable>();

        if (_socketAvailable && socketableObj != null)
        {
            setObj.transform.position = transform.position;
            setObj.transform.rotation = Quaternion.Euler(transform.forward);
            setObj.transform.SetParent(transform);
            socketableObj.Socket(this as ISocket);
            return true;
        }
        else
        {
            return false;
        }
    }
}