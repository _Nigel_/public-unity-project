﻿using UnityEngine;
using System.Collections;
using RTSGame.Players;
using RTSGame.UI;

namespace RTSGame.Units
{
    [RequireComponent(typeof(Renderer))]
    public class UnitInterface : RTSBase, ISelectable, IActivate
    {
        private Renderer unitRenderer;
        private bool _isSelected = false, _canMove = false, _canAttack;
        private IUnitController unitController;
        private IUnit iUnit;
        private bool activated = false;

        public int playersTeam { get { return unitController.team; } }
        public bool canMove { get { return _canMove; } }
        public bool canAttack { get { return _canAttack; } }
        public IUnit unit { get { return iUnit; } }

        protected override bool Initialise()
        {
            return base.Initialise();
        }

        public void Activate()
        {
            if(iUnit == null)
            {
                iUnit = transform.root.GetComponentInChildren<IUnit>();
            }

            if (unitController == null)
            {
                unitController = GetComponentInChildren<IUnitController>();
            }

            if (unitController != null)
            {
                if (GetComponentInChildren<ITransport>() != null)
                {
                    _canMove = true;
                }
                if (GetComponentInChildren<IWeapon>() != null)
                {
                    _canAttack = true;
                }

                unitRenderer = GetComponentInChildren<Renderer>();
            }
            activated = true;
        }

        public void Deactivate()
        {
            activated = false;
        }

        public bool GiveCommand(IUnit target)
        {
            if (activated)
            {
                if (unitController != null && canAttack || canMove)
                {
                    ICommand command = unitController.NewCommand(target);
                    if(command != null)
                    {
                        command.onCommandAccepted += PassReceiveEvent;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool GiveCommand(IDamageable target)
        {
            if (activated)
            {
                if (unitController != null && canAttack)
                {
                    ICommand command = unitController.NewCommand(target);
                    if (command != null)
                    {
                        command.onCommandAccepted += PassReceiveEvent;
                        return true;
                    }
                }
            }
            return false;
        }

        public bool GiveCommand(Vector3 target)
        {
            if (activated)
            {
                if (unitController != null && canMove)
                {
                    ICommand command = unitController.NewCommand(target);
                    if (command != null)
                    {
                        command.onCommandAccepted += PassReceiveEvent;
                        return true;
                    }
                }
            }
            return false;
        }

        public void PassReceiveEvent(ICommand command, Vector3[] path)
        {
            command.onCommandAccepted -= PassReceiveEvent;
            PlayerUI.playerUI.DrawNavigationLine(command, unit, path);
        }

        private void OnBecameVisible()
        {
            if(activated)
            {
                if (iUnit == null)
                {
                    iUnit = transform.root.GetComponentInChildren<IUnit>();
                }

                PlayerUI.playerUI.AddUnit(iUnit);
            }
        }

        private void OnBecameInvisible()
        {
            if(activated && PlayerControls.player != null)
            {
                if (iUnit == null)
                {
                    iUnit = transform.root.GetComponentInChildren<IUnit>();
                }

                if(PlayerUI.playerUI && (System.Object)iUnit != null)
                {
                    PlayerUI.playerUI.RemoveUnit(iUnit);
                }
            }
        }
        
        private void Update()
        {
            if(activated && unitRenderer.isVisible)
            {
                if(PlayerControls.player.team == unitController.team)
                {
                    if (Input.GetMouseButtonUp(0))
                    {
                        Vector3 camPos = Camera.main.WorldToScreenPoint(transform.position);
                        camPos.y = Screen.height - camPos.y;

                        _isSelected = PlayerControls.selectionRect.Contains(camPos);

                        if (_isSelected)
                        {
                            PlayerControls.AddSelected(this as ISelectable);
                        }
                    }
                }
            }
        }
    }
}
