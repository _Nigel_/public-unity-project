﻿using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units.Weapons
{
    public class LazerWeapon : Weapon
    {
        [SerializeField] protected LayerMask m_layerMask;
        [SerializeField] protected GameObject muzzleFlash, hitEffect;
        [SerializeField] protected LineRenderer lazerRenderer;
        [SerializeField, Range(0,1)] protected float lazerDuration;
        
        protected override bool Aim(GameObject possibleTarget)
        {
            transform.LookAt(possibleTarget.transform.position);
            return true;
        }

        protected override IEnumerator ActiveWeaponSequence()
        {
            while (_weaponEnabled)
            {
                if (lastFired < fireRate)
                {
                    lastFired += Time.deltaTime;
                }

                if (lastFired > (fireRate * lazerDuration))
                {
                    lazerRenderer.enabled = false;
                }

                lazerRenderer.SetPosition(0, Vector3.zero);

                if (controller.enemies.Count > 0)
                {
                    if (FindCurrentTarget(controller.enemies))
                    {
                        if (Aim(target) && lastFired >= fireRate)
                        {
                            if (fireType == FireType.Direct)
                            {
                                RaycastHit hit;
                                Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * 500);

                                if (Physics.Raycast(ray, out hit))
                                {
                                    IUnit unit = hit.collider.transform.root.GetComponentInChildren<IUnit>();
                                    if (unit != null && unit.team != base.unit.team)
                                    {
                                        lastFired = 0;
                                        Fire();
                                    }
                                }
                            }
                        }
                    }
                }
                yield return new WaitForFixedUpdate();
            }
            weaponActive = false;

            lazerRenderer.enabled = false;
            weaponActive = false;
        }

        protected override void Fire()
        {
            if(target != null)
            {
                lazerRenderer.enabled = true;
                SoundManager.PlayClipAtPoint(shotSfx, muzzlePoint.transform.position, 1);

                Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * weaponRange);
                RaycastHit hit;

                if(Physics.Raycast(ray, out hit, weaponRange, m_layerMask))
                {
                    IDamageable iDamageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();

                    if (iDamageable != null)
                    {
                        iDamageable.TakeDamage(unit, damage, armorPenetration);
                    }

                    lazerRenderer.SetPosition(1, transform.InverseTransformPoint(hit.point));
                }
                else
                {
                    lazerRenderer.SetPosition(1, transform.InverseTransformPoint(ray.GetPoint(weaponRange)));
                }
            }
        }

        private void OnDestroy()
        {
            if(lazerRenderer != null)
            {
                lazerRenderer.enabled = false;
            }
        }

        protected override bool LineOfSight()
        {
            RaycastHit hit;
            Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * weaponRange);
            if (Physics.Raycast(ray, out hit))
            {
                IUnit unit = hit.collider.GetComponent<IUnit>();
                if (unit != null && unit.team == this.unit.team)
                {
                    Debug.Log("false");
                    return false;
                }

                IDamageable damageable = hit.collider.GetComponent<IDamageable>();
                if (damageable != null)
                {
                    Debug.Log("true");
                    return true;
                }
            }
            Debug.Log("false");
            return false;
        }
    }
}