﻿using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units.Weapons
{
    public class BeamWeapon : Weapon
    {
        [SerializeField] protected LineRenderer lineRenderer;
        [SerializeField] protected AudioClip openBeam, closeBeam;
        [SerializeField] protected bool beamActive = false;
        [SerializeField] protected GameObject hitEffect;

        protected override bool Initialise()
        {
            muzzleFlare.SetActive(false);
            hitEffect.SetActive(false);
            lineRenderer.SetPosition(0, Vector3.zero);
            lineRenderer.enabled = false;
            return base.Initialise();
        }

        protected override IEnumerator ActiveWeaponSequence()
        {
            while (_weaponEnabled)
            {
                if (controller.enemies.Count > 0)
                {
                    if (FindCurrentTarget(controller.enemies))
                    {
                        beamActive = true;

                        yield return StartCoroutine(FireBeam());
                    }
                }

                yield return new WaitForFixedUpdate();
            }
            weaponActive = false;
        }

        protected virtual IEnumerator FireBeam()
        {
            SoundManager.PlayClipAtPoint(openBeam, transform.position, 0.5f);
            AudioSource source = SoundManager.PlayClipAtPoint(openBeam, transform.position, 0.5f, true);
            muzzleFlare.SetActive(true);
            hitEffect.SetActive(true);

            while (beamActive && _weaponEnabled)
            {
                if (Aim(target))
                {
                    if (CanReach(target))
                    {
                        RaycastHit hit;
                        Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * weaponRange);
                        if (Physics.Raycast(ray, out hit))
                        {
                            IDamageable damageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();
                            if (damageable != null)
                            {
                                damageable.TakeDamage(unit, damage, armorPenetration);
                            }
                            else
                            {
                                beamActive = false;
                            }
                        }
                        float propMult = hit.distance * (1 / 10f);
                        lineRenderer.material.SetTextureScale("_MainTex", new Vector2(propMult, 1f));

                        lineRenderer.SetPosition(1, transform.InverseTransformPoint(hit.point));
                        lineRenderer.enabled = true;

                    }
                    else
                    {
                        beamActive = false;
                    }
                }
                else
                {
                    beamActive = false;
                }

                yield return new WaitForFixedUpdate();
            }

            muzzleFlare.SetActive(false);
            hitEffect.SetActive(false);

            lineRenderer.enabled = false;
            source.loop = false;
            source.Stop();
            SoundManager.PlayClipAtPoint(closeBeam, transform.position, 0.5f);
        }
        
        protected override void Fire()
        {

        }
    }
}