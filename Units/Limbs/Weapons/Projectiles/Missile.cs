﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.Weapons
{
    public class Missile : Projectile
    {
        [SerializeField] AudioClip hitSfx;
        [SerializeField] float delayBeforeExplosion;
        [SerializeField] private float velocity = 150f;
        [SerializeField] private float alignSpeed = 5f;
        private float countdown = 0;
        private GameObject target;
        private Vector3 step;

        public void SetTarget(GameObject newTarget)
        {
            target = newTarget;
        }

        protected override void OnCollisionEnter(Collision collision)
        {
            GameObject obj = ObjectPool.GetObject(hitPfx);
            obj.transform.position = transform.position;
            obj.GetComponent<IPoolable>().Activate();
            SoundManager.PlayClipAtPoint(hitSfx, transform.position, 1);

            base.OnCollisionEnter(collision);
        }

        public override void Deactivate()
        {
            countdown = 0;
            base.Deactivate();
        }

        void Update()
        {
            countdown += Time.deltaTime;

            if(countdown > delayBeforeExplosion)
            {
                GameObject obj = ObjectPool.GetObject(hitPfx);
                obj.transform.position = transform.position;
                obj.GetComponent<IPoolable>().Activate();

                Deactivate();
            }

            if(target != null)
            {
                transform.rotation = Quaternion.LookRotation(Vector3.RotateTowards(transform.forward, (target.transform.position - transform.position), Time.deltaTime * alignSpeed, 0.0f));
            }
            
            step = transform.forward * Time.deltaTime * velocity;
            transform.position += step;
        }
    }
}