﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.Weapons
{
    public class Projectile : RTSBase, IPoolable
    {
        public float damage;
        public float impactRadius;
        public float armorPenetration;
        [SerializeField] protected float explosionForce = 1f;
        [HideInInspector] public IUnit creator;
        [SerializeField] protected float duration;
        [SerializeField] protected GameObject hitPfx;
        private Rigidbody _rigidBody;

        public Rigidbody objRigidBody { get { if (_rigidBody != null) _rigidBody = GetComponent<Rigidbody>(); return _rigidBody; } }

        protected override bool Initialise()
        {
            _rigidBody = GetComponent<Rigidbody>();
            return base.Initialise();
        }

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public virtual void Activate(Vector3 position, Vector3 rotation)
        {
            transform.position = position;
            transform.rotation = Quaternion.Euler(rotation);
            _rigidBody.velocity = Vector3.zero;
            gameObject.SetActive(true);
        }

        public virtual void Deactivate()
        {
            _rigidBody.velocity = Vector3.zero;
            gameObject.SetActive(false);
            CancelInvoke("Deactivate");
        }

        protected virtual void OnEnable()
        {
            Invoke("Deactivate", duration);
        }

        protected virtual void OnDisable()
        {
            CancelInvoke("Deactivate");
        }

        protected virtual void OnCollisionEnter(Collision collision)
        {
            GameObject hitEffect = ObjectPool.GetObject(hitPfx);
            hitEffect.transform.position = transform.position;
            hitEffect.GetComponent<IPoolable>().Activate();

            if(impactRadius > 0)
            {
                Collider[] colliders = Physics.OverlapSphere(transform.position, impactRadius);
                
                if(colliders.Length > 0)
                {
                    for(int i=0; i < colliders.Length; i++)
                    {
                        Rigidbody body = colliders[i].transform.root.GetComponentInChildren<Rigidbody>();
                        if(body != null)
                        {
                            body.AddExplosionForce(explosionForce, collision.contacts[0].point, impactRadius);
                        }

                        IDamageable iDamageable = colliders[i].GetComponent<IDamageable>();
                        if (iDamageable != null)
                        {
                            iDamageable.TakeDamage(creator, damage, armorPenetration);
                        }
                    }
                }
            }
            else
            {
                IDamageable iDamageable = collision.collider.GetComponent<IDamageable>();
                if (iDamageable != null)
                {
                    iDamageable.TakeDamage(creator, damage, armorPenetration);
                }
            }

            Deactivate();
        }
    }
}