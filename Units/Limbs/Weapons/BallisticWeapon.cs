﻿using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units.Weapons
{
    public class BallisticWeapon : Weapon
    {
        [SerializeField] protected GameObject muzzleFlash;
        [SerializeField] protected GameObject projectile;
        private float _launchVelocity;

        protected float launchVelocity
        {
            get
            {
                return Mathf.Sqrt(((_weaponRange * -Physics.gravity.y) / Mathf.Sin(2 * 45))) * projectile.GetComponent<Rigidbody>().mass;
            }
        }

        protected float LaunchAngle(float _targetDistance)
        {
                return (Mathf.Rad2Deg * Mathf.Asin( ((_targetDistance * -Physics.gravity.y) / Mathf.Pow(launchVelocity, 2)) ) );
        }

        protected override void Fire()
        {
            GameObject muzPfx = ObjectPool.GetObject(muzzleFlash);
            muzPfx.transform.position = muzzlePoint.transform.position;
            muzPfx.transform.rotation = transform.rotation;
            muzPfx.GetComponent<IPoolable>().Activate();

            GameObject projObj = ObjectPool.GetObject(projectile);
            projObj.transform.position = muzzlePoint.transform.position;
            projObj.transform.rotation = Quaternion.Euler(transform.up);

            Projectile proj = projObj.GetComponent<Projectile>();
            projObj.GetComponent<IPoolable>().Activate();
            proj.damage = damage;
            proj.impactRadius = impactRadius;
            proj.armorPenetration = armorPenetration;
            proj.objRigidBody.AddForce(transform.up.normalized * launchVelocity, ForceMode.Impulse);
        }

        protected override bool Aim(GameObject possibleTarget)
        {
            if(CanFace(possibleTarget))
            {
                Vector3 faceingTarget = (possibleTarget.transform.position - transform.position);
                faceingTarget.y += LaunchAngle(Vector3.Distance(transform.position, possibleTarget.transform.position));
                transform.rotation = Quaternion.Euler(Vector3.RotateTowards(transform.forward, possibleTarget.transform.position - transform.position, turretTurnSpeed * Time.deltaTime, 0.0F));
                return true;
            }
            else
            {
                Vector3 newDir = Vector3.RotateTowards(transform.forward, transform.parent.forward, turretTurnSpeed * Time.deltaTime, 0.0F);
                transform.rotation = Quaternion.LookRotation(newDir);
                return false;
            }
        }
    }
}