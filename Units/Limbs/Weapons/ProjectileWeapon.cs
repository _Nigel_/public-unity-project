﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.Weapons
{
    public class ProjectileWeapon : Weapon
    {
        [SerializeField] protected LayerMask layerMask;
        [SerializeField] GameObject projectile;
        [SerializeField] float launchVelocity;

        protected override void Fire()
        {
            GameObject muzPfx = ObjectPool.GetObject(muzzleFlare);
            muzPfx.transform.position = muzzlePoint.transform.position;
            muzPfx.transform.rotation = transform.rotation;
            muzPfx.GetComponent<IPoolable>().Activate();

            Projectile projObj = ObjectPool.GetObject(projectile).GetComponent<Projectile>();
            projObj.transform.position = muzzlePoint.transform.position;
            projObj.transform.rotation = Quaternion.Euler(transform.up);
            projObj.GetComponent<IPoolable>().Activate();
            projObj.damage = damage;
            projObj.impactRadius = impactRadius;
            projObj.armorPenetration = armorPenetration;
            projObj.creator = unit;
            projObj.objRigidBody.AddForce(transform.forward.normalized * launchVelocity, ForceMode.Impulse);
        }
    }
}