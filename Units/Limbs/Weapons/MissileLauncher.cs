﻿using UnityEngine;
using System.Collections;

namespace RTSGame.Units.Weapons
{
    public class MissileLauncher : Weapon
    {
        public enum MissileType { Homing, Rocket }
        [SerializeField] private GameObject _missile;
        
        protected override void Fire()
        {
            GameObject muzzleFlareObj = ObjectPool.GetObject(muzzleFlare) as GameObject;
            muzzleFlareObj.GetComponent<IPoolable>().Activate();
            muzzleFlareObj.transform.position = muzzlePoint.transform.position;
            muzzleFlareObj.transform.rotation = Quaternion.Euler(muzzlePoint.transform.forward);

            GameObject missileObj = ObjectPool.GetObject(_missile) as GameObject;
            missileObj.transform.position = muzzlePoint.transform.position;
            missileObj.transform.rotation = Quaternion.Euler(muzzlePoint.transform.forward);
            missileObj.GetComponent<IPoolable>().Activate();

            Missile classInst = missileObj.GetComponent<Missile>();
            classInst.SetTarget(target);
            classInst.creator = unit;
        }

        protected override bool Aim(GameObject possibleTarget)
        {
            Ray ray = new Ray(transform.position, (possibleTarget.transform.position - transform.position));
            RaycastHit hit;

            Vector3 newDir;
            if(Physics.Raycast(ray, out hit))
            {
                IUnit iUnit = hit.collider.GetComponent<IUnit>();
                if(iUnit != null && iUnit.team != controller.owner.team)
                {
                    newDir = Vector3.RotateTowards(transform.forward, (possibleTarget.transform.position - transform.position) * 0.5f, turretTurnSpeed * Time.deltaTime, 0.0F);
                    transform.rotation = Quaternion.LookRotation(newDir);
                    return true;
                }
            }

            newDir = Vector3.RotateTowards(transform.forward, transform.up * 0.5f, turretTurnSpeed * Time.deltaTime, 0.0F);
            transform.rotation = Quaternion.LookRotation(newDir);

            return true;
        }

        protected override bool LineOfSight()
        {
            return true;
        }
    }
}