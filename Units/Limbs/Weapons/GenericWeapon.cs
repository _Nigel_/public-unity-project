﻿using UnityEngine;
using System.Collections;
using System;

namespace RTSGame.Units.Weapons
{
    [System.Serializable]
    public class GenericWeapon : Weapon
    {   
        [SerializeField] protected LayerMask m_layerMask;
        [SerializeField] protected GameObject hitEffect;
        protected ParticleSystem muzzleFlash;

        protected override bool Initialise()
        {
            muzzleFlash = transform.GetComponentInChildren<ParticleSystem>();
            return base.Initialise();
        }

        protected override void Fire()
        {
            SoundManager.PlayClipAtPoint(shotSfx, transform.position, .75f);

            RaycastHit hit;
            Ray ray = new Ray(muzzlePoint.transform.position, transform.forward);
            if (Physics.Raycast(ray, out hit, _weaponRange, m_layerMask))
            {
                GameObject hitPfx = ObjectPool.GetObject(hitEffect);
                hitPfx.transform.position = hit.point;
                hitPfx.GetComponent<IPoolable>().Activate();

                IDamageable damageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();
                if (damageable != null)
                {
                    damageable.TakeDamage(unit, damage, armorPenetration);
                }
            }
        }
    }
}