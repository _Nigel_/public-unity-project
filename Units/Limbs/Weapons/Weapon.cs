﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using System.Collections.ObjectModel;
using System.Collections.Generic;

namespace RTSGame.Units.Weapons
{
    public enum FireType { Direct, Indirect }

    public abstract class Weapon : UnitExtension, IWeapon
    {
        protected readonly float aimingDeadzone = 0.1f;
        
        [SerializeField] protected float fireRate, damage, impactRadius, armorPenetration, _weaponRange, turretTurnSpeed;
        [SerializeField, Range(0, 180)] protected float pitchUp, pitchDown, yawLeft, yawRight;
        [SerializeField] protected GameObject muzzlePoint;
        [SerializeField] private float _dangerRating;
        [SerializeField] protected FireType fireType = FireType.Direct;
        [SerializeField] protected AudioClip shotSfx;
        [SerializeField] protected GameObject muzzleFlare;
        protected AudioSource audioSource;
        protected Vector3 defaultRotation;
        protected GameObject target;
        protected bool weaponActive = false, _weaponEnabled;
        protected float lastFired = 0;
        protected IUnitController controller;

        public bool weaponEnabled { get { return _weaponEnabled; } set { _weaponEnabled = value; if (_weaponEnabled) { Activate(); } } }
        public float weaponRange { get { return _weaponRange; } }
        public float weaponDamage { get { return damage; } }
        public float weaponFireRate { get { return fireRate; } }
        public float dangerRating { get { return _dangerRating; } }
        
        public override void Activate()
        {
            _weaponEnabled = true;

            defaultRotation = transform.parent.InverseTransformDirection(transform.forward);

            if (controller == null)
            {
                controller = transform.root.GetComponentInChildren<IUnitController>();
            }

            if (!weaponActive && controller != null)
            {
                weaponActive = true;
                StartCoroutine(ActiveWeaponSequence());
            }

            base.Activate();
        }

        public override void Deactivate()
        {
            _weaponEnabled = false;

            base.Deactivate();
        }
        
        protected virtual IEnumerator ActiveWeaponSequence()
        {
            while (_weaponEnabled)
            {
                if (lastFired < fireRate)
                {
                    lastFired += Time.deltaTime;
                }
                
                if (controller.enemies.Count > 0)
                {
                    if(FindCurrentTarget(controller.enemies))
                    {
                        if (Aim(target) && lastFired >= fireRate)
                        {
                            if (fireType == FireType.Direct)
                            {
                                if (LineOfSight())
                                {
                                    lastFired = 0;
                                    Fire();
                                }
                                
                            }
                        }
                    }
                    else
                    {
                        Vector3 newDir = Vector3.RotateTowards(transform.forward, transform.parent.forward, turretTurnSpeed * Time.deltaTime, 0.0F);
                        transform.rotation = Quaternion.LookRotation(newDir);
                    }
                }

                yield return new WaitForFixedUpdate();
            }
            weaponActive = false;
        }

        protected virtual bool LineOfSight()
        {
            RaycastHit hit;
            Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * weaponRange);
            if (Physics.Raycast(ray, out hit))
            {
                IUnit unit = hit.collider.GetComponent<IUnit>();
                if (unit != null && unit.team == this.unit.team)
                {
                    return false;
                }

                IDamageable damageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();
                if(damageable != null)
                {
                    return true;
                }
            }

            return false;
        }

        protected virtual bool LineOfSight(ref IDamageable damageable)
        {
            RaycastHit hit;
            Ray ray = new Ray(muzzlePoint.transform.position, transform.forward * weaponRange);
            if (Physics.Raycast(ray, out hit))
            {
                IUnit unit = hit.collider.GetComponent<IUnit>();
                if (unit != null && unit.team == this.unit.team)
                {
                    return false;
                }

                damageable = hit.collider.transform.root.GetComponentInChildren<IDamageable>();
                if (damageable != null)
                {
                    return true;
                }
            }

            return false;
        }

        protected bool FindCurrentTarget(ReadOnlyCollection<IUnit> possibleTargets)
        {
            for(int i=0; i < possibleTargets.Count; i++)
            {
                if(CanReach(possibleTargets[i].unitObject))
                {
                    if(CanFace(possibleTargets[i].unitObject))
                    {
                        target = possibleTargets[i].unitObject;
                        return true;
                    }
                }
            }
            return false;
        }
        
        protected bool CanReach(GameObject possibleTarget)
        {
            if(possibleTarget)
            {
                return (transform.position - possibleTarget.transform.position).sqrMagnitude < weaponRange * weaponRange ? true : false;
            }
            return false;
        }

        protected bool CanFace(GameObject possibleTarget)
        {
            Vector3 targetDir = transform.parent.InverseTransformDirection((possibleTarget.transform.position - transform.position));
            Vector3 comparativeDirection = (targetDir - defaultRotation).normalized * 180;

            //Debug.DrawRay(transform.position, transform.parent.TransformDirection(targetDir) * 30, Color.blue);
            //Debug.DrawRay(transform.position, transform.parent.TransformDirection(comparativeDirection) * 30, Color.green);
            //Debug.DrawRay(transform.position, transform.parent.TransformDirection(defaultRotation) * 30, Color.red);

            // Actions for Target Behind
            if(comparativeDirection.z < 1) // target in front actually...
            {
                float flippedX = comparativeDirection.x > 0 ? 90 + ( 90 - (comparativeDirection.x * 0.5f)) : -90 - Mathf.Abs((comparativeDirection.x * 0.5f) + 90);
                float frontY = comparativeDirection.y * 0.5f;

                //if (unit.team == 1)
                //{
                //    Debug.Log(flippedX + " needs to be less than " + yawLeft + " and higher than " + -yawRight);
                //    Debug.Log(frontY + " needs to be less than " + pitchDown + " and higher than " + -pitchUp);
                //}

                if (flippedX < yawLeft && flippedX > -yawRight)
                {
                    if (frontY < pitchDown && frontY > -pitchUp)
                    {
                        return true;
                    }
                }
            }
            // Actions for target in front
            else
            {
                float frontX = comparativeDirection.x * 0.5f;
                float frontY = comparativeDirection.y * 0.5f;

                //if (unit.team == 1)
                //{
                //    Debug.Log(frontX + " needs to be less than " + yawLeft + " and higher than " + -yawRight);
                //    Debug.Log(frontY + " needs to be less than " + pitchDown + " and higher than " + -pitchUp);
                //}

                if (frontX < yawLeft && frontX > -yawRight)
                {
                    if (frontY < pitchDown && frontY > -pitchUp)
                    {
                        return true;
                    }
                }
            }

            return false;
        }

        protected virtual bool Aim(GameObject possibleTarget)
        {
            Vector3 newDir = Vector3.RotateTowards(transform.forward, possibleTarget.transform.position - transform.position, turretTurnSpeed * Time.deltaTime, 0.0F);
            transform.rotation = Quaternion.LookRotation(newDir);
            return true;
        }

        //For testing weapon's forward
        //private void FixedUpdate()
        //{
        //    Debug.DrawRay(transform.position, transform.parent.TransformDirection(defaultRotation) * 30, Color.blue);
        //    Debug.DrawRay(transform.position, transform.up * 10, Color.green);
        //    Debug.DrawRay(transform.position, transform.forward * 20, Color.red);
        //}

        protected abstract void Fire();

        protected override void LevelUp(IUnit unit, int newLevel)
        {
            damage++;
            fireRate = Mathf.Clamp(fireRate - 0.01f, 0.01f, Mathf.Infinity);
        }
    }
}
