﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units.AI
{
    [RequireComponent(typeof(SphereCollider))]
    public class GenericSensor : RTSBase, IActivate
    {
        private IUnitController controller;

        private bool activated = false;

        public void Activate()
        {
            controller = transform.root.GetComponentInChildren<IUnitController>();
            activated = true;
        }

        public void Deactivate()
        {
            activated = false;
        }

        void OnTriggerEnter(Collider other)
        {
            if(activated)
            {
                IUnitController unitController = other.transform.root.GetComponentInChildren<IUnitController>();
                if (unitController != null && controller != null)
                {
                    if (unitController.team != controller.team)
                    {
                        if (unitController.unit != null && !controller.enemies.Contains(unitController.unit) && unitController.unit.health > 0)
                        {
                            controller.AddTarget(unitController.unit);
                        }
                    }
                    else
                    {
                        if (unitController.unit != null && !controller.friendlies.Contains(unitController.unit) && unitController.unit.health > 0)
                        {
                            controller.AddFriendly(unitController.unit);
                        }
                    }
                }
            }
        }

        void OnTriggerExit(Collider other)
        {
            if (activated)
            {
                IUnitController unitController = other.transform.root.GetComponentInChildren<IUnitController>();
                if (unitController != null && controller != null)
                {
                    if (unitController.team != controller.team)
                    {
                        if (unitController.unit != null && controller.enemies.Contains(unitController.unit))
                        {
                            controller.RemoveTarget(unitController.unit);
                        }
                    }
                    else
                    {
                        if (unitController.unit != null && controller.friendlies.Contains(unitController.unit))
                        {
                            controller.RemoveFriendly(unitController.unit);
                        }
                    }
                }
            }
        }
    }
}