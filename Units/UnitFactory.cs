﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace RTSGame.Units
{
    [System.Serializable]
    public class UnitFactory : RTSBase
    {
        private static UnitFactory _unitFactory;
        [SerializeField] private List<GameObject> units = new List<GameObject>();

        protected override bool Initialise()
        {
            _unitFactory = this;
            return base.Initialise();
        }

        public static UnitFactory unitFactory
        {
            get
            {
                if(_unitFactory == null)
                {
                    GameObject obj = Resources.Load("UnitFactory") as GameObject;
                    _unitFactory = obj.GetComponent<UnitFactory>();
                }

                return _unitFactory;
            }
        }

        public static GameObject CreateUnit(int id)
        {
            return Instantiate(unitFactory.units[id], Vector3.zero, Quaternion.identity) as GameObject;
        }

        public static GameObject CreateUnit(string prefabName)
        {
            for(int i=0; i < unitFactory.units.Count; i++)
            {
                if(unitFactory.units[i].name == prefabName)
                {
                    GameObject obj = Instantiate(unitFactory.units[i], Vector3.zero, Quaternion.identity) as GameObject;
                    return obj;
                }
            }
            return null;
        }
        
        public static GameObject CreateUnit(string prefabName, int team)
        {
            for (int i = 0; i < unitFactory.units.Count; i++)
            {
                if (unitFactory.units[i].name == prefabName)
                {
                    GameObject obj = Instantiate(unitFactory.units[i], Vector3.zero, Quaternion.identity) as GameObject;
                    obj.transform.root.GetComponentInChildren<IUnitController>().SetTeam(team);
                    return obj;
                }
            }
            return null;
        }

        public static void AddUnit(GameObject newUnit)
        {
            unitFactory.units.Add(newUnit);
        }
    }
}