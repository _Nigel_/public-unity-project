﻿using UnityEngine;
using System.Collections;

public interface IEnergyCost
{
    float creationCost { get; }
    float energyCost { get; }
}