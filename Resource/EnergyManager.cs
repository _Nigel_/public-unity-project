﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;

namespace RTSGame.Resource
{
    public class EnergyManager : RTSBase, ICapacitor
    {
        [SerializeField] private AnimationCurve rechargeRateCurve;
        [SerializeField] private float _capacity = 100;
        [SerializeField] private float rechargeScale = 10;
        [SerializeField] private float energyLoss = 0;
        [SerializeField] private float _energy = 0;
        private float chargeRate;

        public float capacity { get { return _capacity; } }
        public float energy { get { return _energy; } }
        
        void Update()
        {
            if(_energy < _capacity)
            {
                chargeRate = rechargeRateCurve.Evaluate(1 - (energyLoss / _capacity)) * Time.deltaTime * rechargeScale;
                _energy = Mathf.Clamp(_energy + chargeRate, 0, _capacity);
            }
        }

        public bool AvailableEnergy(float energyNeeded)
        {
            return true;
        }

        public bool AvailableCapacity(float energyNeeded)
        {
            return true;
        }

        public void EnergyOutput(float amount)
        {
            energyLoss += amount;
        }

        public bool UseEnergy(float amountUsed)
        {
            if(_energy > amountUsed)
            {
                _energy -= amountUsed;
                return true;
            }
            else
            {
                return false;
            }
        }
    }
}