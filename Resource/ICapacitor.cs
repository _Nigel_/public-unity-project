﻿using UnityEngine;
using System.Collections;

public interface ICapacitor
{
    float capacity { get; }
    float energy { get; }
    bool AvailableEnergy(float amountNeeded);
    bool AvailableCapacity(float amountNeeded);
    void EnergyOutput(float addedOutput);
    bool UseEnergy(float amountUsed);
}
