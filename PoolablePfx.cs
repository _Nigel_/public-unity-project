﻿using UnityEngine;
using System.Collections;

namespace RTSGame
{
    public class PoolablePfx : RTSBase, IPoolable
    {
        [SerializeField] private float liveDuration;

        private void OnEnable()
        {
            Invoke("Deactivate", liveDuration);
        }

        private void OnDisable()
        {
            CancelInvoke("Deactivate");
        }

        public void Activate()
        {
            gameObject.SetActive(true);
        }

        public void Activate(Vector3 position, Vector3 rotation)
        {
            transform.position = position;
            transform.rotation = Quaternion.Euler(rotation);
            gameObject.SetActive(true);
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
        }
    }
}
