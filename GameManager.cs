﻿#pragma warning disable 0649

using System.Collections;
using UnityEngine;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using RTSGame.Players;

namespace RTSGame
{
    public enum GameType { Annihilation, DestroyTheCommander, Campaign }

    public class GameManager : RTSBase
    {
        private static GameManager _gameManager;
        [SerializeField] private List<IPlayer> _players = new List<IPlayer>();
        [SerializeField] private GameObject humanPlayer, virtualPlayer;
        [SerializeField] private GameType _gameType;

        public GameType gameType { get { return _gameType; } }
        public static ReadOnlyCollection<IPlayer> players { get { return gameManager._players.AsReadOnly(); } }
        
        public static GameManager gameManager
        {
            get
            {
                if(_gameManager == null)
                {
                    GameObject obj;
                    if (!(obj = GameObject.Find("GameManager")))
                    {
                        obj = new GameObject("GameManager");
                        obj.AddComponent<GameManager>();
                    }
                    
                    _gameManager = obj.GetComponent<GameManager>();
                }

                return _gameManager;
            }
        }

        protected override bool Initialise()
        {
            _gameManager = this;
            
            if (gameType == GameType.Campaign)
            {
                StartGame();
            }
            else
            {
                StartGame(new PlayerType[] { PlayerType.Local_Human, PlayerType.Virtual, PlayerType.Virtual });
            }

            return base.Initialise();
        }

        public void StartGame()
        {
            if (_players.Count > 0)
            {
                for (int i = 0; i < _players.Count; i++)
                {
                    _players[i].PlayerSetup(i);
                }
            }

            SortPlayers();
        }

        public void StartGame(PlayerType[] newPlayers)
        {
            List<GameObject> spawnPoints = new List<GameObject>();
            spawnPoints.AddRange(GameObject.FindGameObjectsWithTag("SpawnPoint"));

            if (spawnPoints.Count < newPlayers.Length)
            {
                Debug.Log("Unable to start game! Too many player's listed to spawn!");
            }

            for (int i = 0; i < newPlayers.Length; i++)
            {
                GameObject playerObj = null;

                switch (newPlayers[i])
                {
                    case PlayerType.Local_Human:
                        {
                            playerObj = Instantiate(humanPlayer, spawnPoints[i].transform.position, spawnPoints[i].transform.rotation) as GameObject;
                            playerObj.GetComponent<IPlayer>().PlayerSetup(i);
                            break;
                        }
                    case PlayerType.External_Human:
                        {
                            break;
                        }
                    case PlayerType.Virtual:
                        {
                            playerObj = Instantiate(virtualPlayer, spawnPoints[i].transform.position, spawnPoints[i].transform.rotation) as GameObject;
                            playerObj.GetComponent<IPlayer>().PlayerSetup(i);
                            break;
                        }
                }

                _players.Add(playerObj.GetComponent<IPlayer>());
            }
        }

        public static void AddPlayer(IPlayer newPlayer)
        {
            if(!gameManager._players.Contains(newPlayer))
            {
                gameManager._players.Add(newPlayer);
            }

            //gameManager.SortPlayers();
        }

        protected void SortPlayers()
        {
            _players.Sort(delegate (IPlayer a, IPlayer b)
            {
                return (a.playerID.CompareTo(b.playerID));
            });
        }
    }
}